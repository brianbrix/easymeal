import base64
import os
from datetime import datetime

import six
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
from cryptography.fernet import Fernet
from django.core import signing
from django.db import IntegrityError, transaction
from django.db.models.signals import post_save
from django.dispatch import receiver, Signal
from django.forms import model_to_dict
from rest_framework.exceptions import ValidationError

from cafeteria.models import UserPass, PassRefNumber
from foodicious.settings import USER_ROLES

import requests

from hotel.models import OrderNumber, Order
from store.models import RequisitionUpdate, RequisitionIssuance
from transaction.models import Transaction, OrderTransaction, Invoice
from transaction.utils import makePayment, makePassPayment
from utils.c import here, load_key, encrypt


def get_serializer_data(model, obj, serializer):
    d = model.objects.filter(pk=obj.id)
    ser = serializer(instance=d, many=True)
    if not ser.data:
        return
    return ser.data[0]


def roles_available(request, roles):
    return all([i in USER_ROLES(request) for i in roles])


def make_payment(amount, user_phone, transaction_summary, account_ref):
    try:
        r = requests.post("https://uptechpay.com/api/getaccess",
                          data={"email": "brianmokandu35@gmail.com", "password": "ngojakiasi"})
        if r.status_code == 200:
            token = r.json()["access_token"]
            headers = {'Authorization': f'Bearer {token}', 'Content-Type': 'application/json'}
            r2 = requests.post("https://uptechpay.com/api/makepayment",
                               json=
                               {
                                   "amount": amount,
                                   "user_phone": user_phone,
                                   "transaction_summary": transaction_summary,
                                   "account_ref": account_ref,
                                   "api_key": "RVRFUktCblppTXJjQ3lKalpwOUNpUmkwQ0hZM09YRGdicmlhbm1va2FuZHUzNUBnbWFpbC5jb20="
                               },
                               headers=headers
                               )
            # print(r2.text)
            return r2
    except Exception as e:
        print(str(e))
        return {"error": str(e)}


@receiver(post_save, sender=Order)
def generate_order_number(instance, created, *, raw=False, **kwargs):
    if created:
        if instance.status == "AWAITING_PROCESSING":
            try:
                latest = OrderNumber.objects.latest('created')
                ord = int(str(latest.order_number).split("#")[1])
                now_date = datetime.strptime(datetime.now().strftime('%Y-%m-%d'), '%Y-%m-%d')
                last_date = datetime.strptime(latest.created.strftime('%Y-%m-%d'), '%Y-%m-%d')
                if now_date > last_date:
                    order_no = f"ORD-{str(datetime.now().strftime('%Y%m%d'))}#{1}"
                else:
                    order_no = f"ORD-{str(datetime.now().strftime('%Y%m%d'))}#{ord + 1}"
            except OrderNumber.DoesNotExist:
                order_no = f"ORD-{str(datetime.now().strftime('%Y%m%d'))}#{1}"
            except IntegrityError:
                order_no = f"ORD-{str(datetime.now().strftime('%Y%m%d'))}#{1}"

            ord = OrderNumber.objects.create(order_number=order_no)
            instance.order_number = order_no
            print(instance)
            print("ordd", instance.order_number)

            makePayment(instance)
    # return instance


@receiver(post_save, sender=RequisitionUpdate)
def update_requisition(instance, created, *, raw=False, **kwargs):
    if instance.quantity_given_last is not None:
        RequisitionIssuance.objects.create(requisition_record=instance, quantity_given=instance.quantity_given_last,
                                           quantity_remaining=instance.quantity_remaining)


@receiver(post_save, sender=Invoice)
def update_invoice_transaction(instance, created, *, raw=False, **kwargs):
    if instance.status == "CONFIRMED":
        if instance.order_type == "MEAL_PASS":
            instance.status = "PAID"
            instance.save()
            t = Transaction.objects.create(amount=instance.amount, phone_number=instance.meal_pass.phone_number,
                                           bill_ref_no=instance.invoice_number, invoice=instance)
            t.full_clean()
        if instance.order_type == "RESTAURANT":
            instance.status = "PAID"
            instance.save()
            t = Transaction.objects.create(amount=instance.amount, phone_number=instance.order.phone_number,
                                           bill_ref_no=instance.invoice_number, invoice=instance)
            t.full_clean()


def custom_validation_for_fields(fields, data):
    errors = []
    for x in fields:
        if x not in data:
            errors.append({x: f"The required field {x} is missing."})
    if len(errors) > 0:
        raise ValidationError({"details": errors})


@receiver(post_save, sender=UserPass)
def generate_pass_number(instance, created, *, raw=False, **kwargs):
    if created:
        if instance.status =="PENDING":
            try:

                latest = PassRefNumber.objects.latest('created')
                ord = int(str(latest.pass_ref_number).split("#")[1])
                now_date = datetime.strptime(datetime.now().strftime('%Y-%m-%d'), '%Y-%m-%d')
                last_date = datetime.strptime(latest.created.strftime('%Y-%m-%d'), '%Y-%m-%d')
                if now_date > last_date:
                    pass_ref_number = f"MPS-{str(datetime.now().strftime('%Y%m%d'))}#{1}"
                else:
                    pass_ref_number = f"MPS-{str(datetime.now().strftime('%Y%m%d'))}#{ord + 1}"
            except PassRefNumber.DoesNotExist:
                pass_ref_number = f"MPS-{str(datetime.now().strftime('%Y%m%d'))}#{1}"
            except IntegrityError:
                pass_ref_number = f"MPS-{str(datetime.now().strftime('%Y%m%d'))}#{1}"

            ord = PassRefNumber.objects.create(pass_ref_number=pass_ref_number)
            instance.pass_ref_number = pass_ref_number
            instance.meal_pass.quantity_remaining -= 1
            instance.meal_pass.save()
            print(instance)
            print("ordd", instance.pass_ref_number)
            makePassPayment(instance)
    # return instance
