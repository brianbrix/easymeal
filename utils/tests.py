class VerifyOrder(APIView):
    permission_classes = (IsWaiter | IsAdmin | IsChef,)
    serializer_class = OrderSerializer

    def post(self, request, *args, **kwargs):
        if "signature" not in request.data:
            return Response({"details": "You must pass the unique id for the order."},
                            status=status.HTTP_400_BAD_REQUEST)
        if "order_id" not in request.data:
            return Response({"details": "You must pass the unique id for the order."},
                            status=status.HTTP_400_BAD_REQUEST)

        try:

            decrypted_order = ast.literal_eval(decrypt_message(request.data["signature"].encode()).decode())
        except cryptography.fernet.InvalidToken:
            return Response({"error": "The order is could not be verified."}, status=status.HTTP_400_BAD_REQUEST)

        queryset = get_object_or_404(Order, pk=request.data["order_id"], signature=request.data["signature"])
        today_last_week = datetime.datetime.now() - datetime.timedelta(days=7)
        if queryset.created < today_last_week:
            return Response({"error": "The order is expired."}, status=status.HTTP_400_BAD_REQUEST)
        d = {str(k): str(v) for k, v in model_to_dict(queryset).items() if str(k) != "signature"}
        if "order_number" in decrypted_order:
            if d["order_number"] == decrypted_order["order_number"]:
                serializer_class = OrderSerializer(instance=queryset, context={'request': self.request})

                return Response({"order": serializer_class.data}, status=status.HTTP_200_OK)
        else:
            return Response({"error": "The order is could not be verified."}, status=status.HTTP_400_BAD_REQUEST)


class AddSig(UpdateAPIView):
    permission_classes = (IsWaiter | IsAdmin | IsChef,)
    serializer_class = OrderSerializer

    def update(self, request, *args, **kwargs):
        # today_last_week = datetime.datetime.now() - datetime.timedelta(days=7)
        # ordrs = Order.objects.filter(created__gt=today_last_week, created__lt=datetime.datetime.now())
        order = get_object_or_404(Order, pk=kwargs["pk"])
        d = str({str(k): str(v) for k, v in model_to_dict(order).items() if str(k) != "signature"})
        order.signature = encrypt(d).decode()
        order.save()
        # with transaction.atomic():
        #     for i in ordrs:
        #         d = str({str(k): str(v) for k, v in model_to_dict(i).items() if str(k) != "signature"})
        #         i.signature = encrypt(d).decode()
        #         i.save()
        return Response({"details": "Signature refreshed."})