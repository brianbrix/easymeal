from rest_framework import serializers

from utils.models import ImageUpload


class ImageUploadSerializer(serializers.ModelSerializer):
    image = serializers.ImageField(write_only=False, required=True)

    class Meta:
        model = ImageUpload
        fields = '__all__'
