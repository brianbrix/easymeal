from django.shortcuts import render

# Create your views here.
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.schemas import SchemaGenerator
from rest_framework.views import APIView
from rest_framework_swagger import renderers

from utils.models import ImageUpload
from utils.permissions import IsUser, IsAdmin, IsChef, IsWaiter
from utils.serializers import ImageUploadSerializer


class SwaggerSchemaView(APIView):
    permission_classes = [AllowAny]
    renderer_classes = [
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer
    ]

    def get(self, request):
        generator = SchemaGenerator()
        schema = generator.get_schema()
        return Response(schema)


class ImageUploadView(CreateAPIView):
    permission_classes = (AllowAny,)
    queryset = ImageUpload.objects.all()
    serializer_class = ImageUploadSerializer


@api_view(['GET'])
@permission_classes((IsUser | IsAdmin | IsChef | IsWaiter,))
def get_roles(request):
    return Response({"roles": request.user.roles.values_list(flat=True)})
