import os
from datetime import timedelta, datetime

from cryptography.fernet import Fernet
from django.db import transaction
from django.forms import model_to_dict

from hotel.models import Order

here = os.path.dirname(os.path.abspath(__file__))


def get_model_fields(model):
    return [field.name for field in model._meta.fields]


def load_key():
    """
    Load the previously generated key
    """
    return open(os.path.join(here, "secret.key"), "rb").read()


def encrypt(message):
    key = load_key()
    encoded_message = message.encode()
    f = Fernet(key)
    encrypted_message = f.encrypt(encoded_message)
    return encrypted_message


def decrypt_message(encrypted_message):
    """
    Decrypts an encrypted message
    """
    key = load_key()
    f = Fernet(key)
    decrypted_message = f.decrypt(encrypted_message)

    return decrypted_message


def update_orders():
    today_last_week = datetime.now() - timedelta(days=7)
    ordrs = Order.objects.filter(created__gt=today_last_week, created__lt=datetime.now())
    with transaction.atomic():
        for i in ordrs:
            d = str({str(k): str(v) for k, v in model_to_dict(i).items() if str(k) != "signature"})
            i.signature = encrypt(d).decode()
            i.save()


def generate_keys():
    key = Fernet.generate_key()
    with open(os.path.join(here, "secret.key"), "wb") as key_file:
        key_file.write(key)
    update_orders()


