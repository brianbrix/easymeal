from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from custom_auth.views import MyUserRolesView
from .views import *


urlpatterns = [
    path('upload/', ImageUploadView.as_view()),
    path('roles/', get_roles),
    path('my-roles/', MyUserRolesView.as_view()),

]
