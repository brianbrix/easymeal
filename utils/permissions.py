from rest_framework import permissions

from foodicious.settings import USER_ROLES


class IsAuthenticated(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated and request.user.verified:
            return True
        return False


class IsAdmin(permissions.BasePermission):
    def has_permission(self, request, view):

        if request.user.is_authenticated and request.user.verified:
            if "ADMIN" in USER_ROLES(request):
                return True
        return False


class IsChef(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated and request.user.verified:
            if "CHEF" in USER_ROLES(request):
                return True
        return False


class IsStoreKeeper(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated and request.user.verified:
            if "STOREKEEPER" in USER_ROLES(request):
                return True
        return False


class IsUser(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated and request.user.verified:
            if "USER" in USER_ROLES(request):
                return True
        return False


class IsWaiter(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated and request.user.verified:
            if "WAITER" in USER_ROLES(request):
                return True
        return False
