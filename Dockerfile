# base image
FROM python:3.8
# setup environment variable
ENV DockerHOME=/home/app/webapp

# set work directory
RUN mkdir -p $DockerHOME

# where your code lives
WORKDIR $DockerHOME

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
# RUN apt update
# RUN apt-get -y install gcc
# RUN apt-get -y install python3-dev
#    && apt install gcc python3-dev musl-dev

# install dependencies

RUN pip install --upgrade pip

RUN pip install --upgrade wheel
# copy whole project to your docker home directory.
COPY . $DockerHOME
# run this command to install all dependencies
# RUN pip install -r req.txt
RUN pip install -r requirements.txt
# port where the Django app runs
#EXPOSE 8000
# start server
#CMD python manage.py runserver