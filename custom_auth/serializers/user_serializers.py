from datetime import timedelta, datetime

from django.contrib.auth.models import update_last_login
from django.contrib.auth.password_validation import validate_password
from django.core.mail import EmailMultiAlternatives
from django.db.models.base import Model
from django_otp import devices_for_user
from django_otp.plugins.otp_totp.models import TOTPDevice
from rest_framework import serializers, status
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.validators import UniqueValidator
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.tokens import RefreshToken

from custom_auth.models import User, Role, Validation
from foodicious.settings import MEDIA_URL, MEDIA_ROOT
from utils.models import TOTPVerification
from django.conf import settings
from django.core.mail import send_mail


def get_user_totp_device(user, confirmed=None):
    devices = devices_for_user(user, confirmed=confirmed)
    for device in devices:
        if isinstance(device, TOTPDevice):
            return device


class UserSerializer(serializers.ModelSerializer):
    # roles = serializers.SerializerMethodField('get_roles')

    class Meta:
        fields = ('id', 'name', 'email', 'phone_number', 'date_joined')
        model = User

    # def get_roles(self, user):
    #     return user.roles.values_list(flat=True)


class RolesSerializer(serializers.ModelSerializer):
    role = serializers.CharField(required=True, write_only=True)

    class Meta:
        model = Role
        fields = "__all__"


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

    def validate(self, attrs):
        credentials = {
            'email': '',
            'password': attrs.get("password")
        }
        user_obj = User.objects.filter(email=attrs.get("email")).first() or User.objects.filter(
            username=attrs.get("email")).first()
        if user_obj:
            if not user_obj.verified:
                raise serializers.ValidationError({"error": "not verified"})
            credentials['email'] = user_obj.email
        data = super().validate(credentials)

        # Add extra responses here
        data['email'] = self.user.email
        data['roles'] = self.user.roles.values_list('role', flat=True)
        update_last_login(None, self.user)
        return data


def send_token(validated_data, user, reg=True):
    code = TOTPVerification()
    token = code.generate_token()
    if code.verify_token(token):

        html_content = f"Please enter the following validation code: {str(token)}"

        msg = EmailMultiAlternatives("Easymeal services", "text_content", 'easymealj@gmail.com', [validated_data['email']])

        msg.attach_alternative(html_content, "text/html")
        try:
            msg.send(fail_silently=False)
        except Exception as e:
            print(e)
            
#         subject = 'Welcome to DataFlair'
#         message = f'Hope you are enjoying your Django Tutorials: {str(token)}'
#         recepient = [validated_data['email']]
#         send_mail(subject, 
#             message, 'easymealj@gmail.com', [recepient], fail_silently = False)
        

        if reg:
            try:
                v = Validation.objects.get(user=user, for_reg=True)
                v.validation_code = token
                v.updated_on = datetime.now()
                v.save()
            except Validation.DoesNotExist:
                v = Validation.objects.create(
                    validation_code=token,
                    user=user
                )
        else:
            try:
                v = Validation.objects.get(user=user, for_reg=False)
                v.validation_code = token
                v.updated_on = datetime.now()
                v.save()
                user.update_verified = False
                user.save()
            except Validation.DoesNotExist:
                v = Validation.objects.create(
                    validation_code=token,
                    user=user,
                    for_reg=False
                )
                user.update_verified = False
                user.save()


class RegisterSerializer(serializers.ModelSerializer):
    """
    {
    Content-Type:application/json
"roles":[1, 2],
"password":"ngojakiasi",
"password2":"ngojakiasi",
"email":"mokandubrian3@gmail.com",
"phone_number":"0745865323"
}
    """
    # roles = serializers.PrimaryKeyRelatedField(queryset=Role.objects.all(), many=True, write_only=True)
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    phone_number = serializers.CharField(write_only=False, required=True)
    id = serializers.UUIDField(write_only=False, required=False)
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)
    name = serializers.CharField(write_only=False, required=True)
    validation_code = serializers.IntegerField(write_only=False, required=False)
    avatar = serializers.ImageField(write_only=False, required=False)

    class Meta:
        model = User
        fields = (
            'password', 'password2', 'email', 'phone_number', 'name', 'avatar', 'validation_code', 'verified', 'id')
        # exclude = ('verified',)
        extra_kwargs = {
            'email': {'required': True},
            'phone_number': {'required': True},
            'roles': {'required': True},
        }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})
        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data['email'],
            phone_number=validated_data['phone_number'],
            name=validated_data['name'],
            # avatar=validated_data['avatar'],
        )
        if 'verified' in validated_data:
            validated_data.pop('verified')  # validated_data no longer has member
        if 'roles' in validated_data:
            validated_data.pop('roles')
        user.set_password(validated_data['password'])
        user.save()
        if "avatar" in validated_data:
            user.avatar = validated_data['avatar']
        user.roles.set(["USER"])
        send_token(validated_data, user)
        user.save()

        return user


class RegisterStaffSerializer(serializers.ModelSerializer):
    """
    {
    Content-Type:application/json
"roles":[1, 2],
"password":"ngojakiasi",
"password2":"ngojakiasi",
"email":"mokandubrian3@gmail.com",
"phone_number":"0745865323"
}
    """
    roles = serializers.PrimaryKeyRelatedField(queryset=Role.objects.all(), many=True, write_only=True)
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    phone_number = serializers.CharField(write_only=False, required=True)
    id = serializers.UUIDField(write_only=False, required=False)
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)
    name = serializers.CharField(write_only=False, required=True)
    validation_code = serializers.IntegerField(write_only=False, required=False)
    avatar = serializers.ImageField(write_only=False, required=False)

    class Meta:
        model = User
        fields = (
            'password', 'password2', 'email', 'phone_number', 'roles', 'name', 'avatar', 'validation_code', 'verified',
            'id')
        # exclude = ('verified',)
        extra_kwargs = {
            'email': {'required': True},
            'phone_number': {'required': True},
            'roles': {'required': True},
        }

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})
        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data['email'],
            phone_number=validated_data['phone_number'],
            name=validated_data['name'],
            # avatar=validated_data['avatar'],
        )
        if 'verified' in validated_data:
            validated_data.pop('verified')  # validated_data no longer has member
        user.set_password(validated_data['password'])
        user.save()
        if "avatar" in validated_data:
            user.avatar = validated_data['avatar']
        user.roles.set(validated_data["roles"])
        send_token(validated_data, user)
        user.save()

        return user

    # def update(self, instance, validated_data):
    #     instance.roles.set(validated_data["roles"])
    #     send_token(validated_data, user)
    #     user.save()


class ChangePasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)
    old_password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('old_password', 'password', 'password2')

    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def validate_old_password(self, value):
        user = self.context['request'].user
        if not user.check_password(value):
            raise serializers.ValidationError({"old_password": "Old password is not correct"})
        return value

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password'])
        instance.save()

        return instance


class ResetPasswordSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)
    token = serializers.IntegerField(write_only=True, required=True)
    email = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ('token', 'password', 'password2', 'email')

    def save(self):
        if self.validated_data['password'] != self.validated_data['password2']:
            raise serializers.ValidationError({"password": "Password fields didn't match."})
        if "token" not in self.validated_data or not self.validated_data['token']:
            raise serializers.ValidationError({"token": "Token field is required"})
        if User.objects.filter(email=self.validated_data['email']).exists():
            user = User.objects.get(email=self.validated_data['email'])
            if user.update_verified:
                raise serializers.ValidationError({"token": "User token is invalid."})
            try:
                v = Validation.objects.get(user_id=user.id, for_reg=False, validation_code=self.validated_data['token'])

                expiry = v.updated_on + timedelta(seconds=3600)
                if expiry >= v.created_on:
                    user.update_verified = True
                    user.set_password(self.validated_data['password'])
                    user.save()
                    return user
                raise serializers.ValidationError({"token": "Token is already expired."})
            except Validation.DoesNotExist:
                raise serializers.ValidationError({"token": "Invalid token"})

        raise serializers.ValidationError({"user": "User does not exist"})

    # def validate_token(self, value):
    #     user = self.context['request'].user
    #     if not user.check_password(value):
    #         raise serializers.ValidationError({"old_password": "Old password is not correct"})
    #     return value

    # def update(self, instance, validated_data):
    #     instance.set_password(validated_data['password'])
    #     instance.save()
    #
    #     return instance


class UpdateUserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=True)

    class Meta:
        model = User
        fields = ('email', 'phone_number')
        extra_kwargs = {
            'email': {'required': True},
            'phone_number': {'required': True},
        }

    def update(self, instance, validated_data):
        user = self.context['request'].user
        if user.id != instance.pk:
            raise serializers.ValidationError({"authorize": "You don't have permission for this user."})
        instance.email = validated_data['email']
        instance.phone_number = validated_data['phone_number']
        instance.save()
        return instance
