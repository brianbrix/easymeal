import os
import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.deconstruct import deconstructible

from foodicious.settings import MEDIA_ROOT, MEDIA_URL
from utils.models import TimeStampedModel, UploadToPathAndRename, ImageUpload


class Role(models.Model):
    """
  The Role entries are managed by the system,
  automatically created via a Django data migration.
  """
    ROLE_CHOICES = (
        ("USER", 'USER'),
        ("CHEF", 'CHEF'),
        ("STOREKEEPER", 'STOREKEEPER'),
        ("WAITER", 'WAITER'),
        ("ADMIN", 'ADMIN'),
    )

    role = models.CharField(choices=ROLE_CHOICES, unique=True, max_length=255, primary_key=True)

    class Meta:
        app_label = "custom_auth"

    def __str__(self):
        return self.get_role_display()


class User(AbstractUser):
    REQUIRED_FIELDS = ['username']
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    roles = models.ManyToManyField(Role)
    email = models.EmailField(unique=True)
    phone_number = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    name = models.CharField(max_length=90)
    avatar = models.ForeignKey(ImageUpload, null=True, on_delete=models.CASCADE)
    verified = models.BooleanField(default=False)
    update_verified = models.BooleanField(default=False)

    username = models.CharField(max_length=30, unique=True, null=True)
    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email


class Validation(TimeStampedModel):
    validation_code = models.IntegerField(null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    for_reg = models.BooleanField(default=True)
