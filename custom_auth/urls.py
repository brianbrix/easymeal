from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from .views import *

urlpatterns = [
    path('login/', MyObtainTokenPairView.as_view()),
    path('isAuthenticated/', IsAuthenticatedView.as_view()),
    path('login/refresh/', TokenRefreshView.as_view()),
    path('register/', RegisterView.as_view({"post": "create"})),
    path('otp-verify/', RegisterView.as_view({"post": "verify_token"})),
    path('update-profile/', UpdateProfileView.as_view(), name="update"),
    path('reset-password/', ResPassView.as_view()),
    path('my-profile/', UserProfileView.as_view()),
    path('update-password/', ResetPasswordView.as_view()),


]
