from rest_framework import status, serializers
from rest_framework.exceptions import ValidationError
from rest_framework.views import exception_handler
from django.http import Http404, HttpResponseBadRequest


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    if isinstance(exc, Http404):
        custom_response_data = {
            'detail': 'This object does not exist.'  # custom exception message
        }
        if isinstance(exc, HttpResponseBadRequest):
            custom_response_data = {
                'detail': 'Invalid details given.'  # custom exception message
            }
        response.data = custom_response_data  # set the custom response data on response object

    return response


class CustomAPIException(ValidationError):
    """
    raises API exceptions with custom messages and custom status codes
    """
    status_code = status.HTTP_400_BAD_REQUEST
    default_code = 'error'

    def __init__(self, detail, status_code=None):
        self.detail = detail
        if status_code is not None:
            self.status_code = status_code


def custom_error(*args):
    for field in args:
        for key, error in field.error_messages.items():
            pass
