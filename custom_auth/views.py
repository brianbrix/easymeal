import random
import string
from datetime import timedelta

from django.contrib.auth.decorators import permission_required
from django.core.exceptions import ValidationError
from django.db import transaction
from django.db.utils import IntegrityError
from django.forms import model_to_dict
from django.http import JsonResponse
from django.middleware import csrf
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from rest_framework import generics
from rest_framework.generics import CreateAPIView, ListAPIView, UpdateAPIView, get_object_or_404
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from custom_auth.serializers.user_serializers import *
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_simplejwt.views import TokenObtainPairView

from myadmin.models import Department
from myadmin.serializers import StaffMemberSerializer
from utils.permissions import IsUser, IsAdmin, IsChef, IsWaiter


class MyObtainTokenPairView(TokenObtainPairView):
    permission_classes = (AllowAny,)
    serializer_class = MyTokenObtainPairSerializer


class RegisterView(ModelViewSet):
    # queryset = User.objects.all()
    # print(queryset)
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer

    @transaction.atomic
    def verify_token(self, request, *args, **kwargs):
        if "token" not in request.data or not request.data.get('token'):
            return Response({"token": "Token field is required"}, status=status.HTTP_400_BAD_REQUEST)
        try:
            user = User.objects.get(email=request.data.get('email'))
            if user.verified:
                return Response({"token": "User is already verified."}, status=status.HTTP_400_BAD_REQUEST)
            try:
                v = Validation.objects.get(user_id=user.id, for_reg=True, validation_code=request.data.get('token'))
                if v:
                    expiry = v.modified + timedelta(seconds=3600)
                    if expiry >= v.created:
                        user.verified = True
                        user.save()
                        return Response({"token": "Token was successfully verified."}, status=status.HTTP_200_OK)
            except Validation.DoesNotExist:
                return Response({"token": "Invalid details given."}, status=status.HTTP_400_BAD_REQUEST)

            return Response({"token": "Invalid token"}, status=status.HTTP_400_BAD_REQUEST)
        except User.DoesNotExist:
            return Response({"token": "User does not exist"}, status=status.HTTP_400_BAD_REQUEST)


class RegisterStaff(ModelViewSet):
    # queryset = User.objects.all()
    # print(queryset)
    permission_classes = (IsAdmin | IsChef,)
    permission_classes = (AllowAny,)
    serializer_class = RegisterStaffSerializer

    @transaction.atomic
    def post(self, request):
        try:
            fields = ["department", "role", "title", "on_duty"]
            errors = []
            for x in fields:

                if x not in request.data:
                    errors.append({x: f"The required field {x} is missing."})
            if len(errors) > 0:
                return Response({"details": errors}, status=HTTP_400_BAD_REQUEST)
            passw = ''.join([random.choice(string.digits + string.ascii_uppercase) for i in range(0, 10)])
            request.data["password"] = passw
            request.data["password2"] = passw
            request.data["roles"] = [request.data["role"]]
            serializer = RegisterStaffSerializer(data=request.data)

            if not serializer.is_valid():
                return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
            id = serializer.create(validated_data=request.data).id
            u = User.objects.get(id=id)
            try:
                if not u:
                    return Response({"error": "Unable to create user"}, status=HTTP_400_BAD_REQUEST)
                # d = get_object_or_404(Department, id=request.data["department"])
                d = Department.objects.get(id=request.data["department"])
                if not d:
                    return Response({"error": "Invalid department specified."}, status=HTTP_400_BAD_REQUEST)
                # r = get_object_or_404(Role, role=request.data["role"])
                r = Role.objects.get(role=request.data["role"])
                if not r:
                    return Response({"error": "Invalid role specified"}, status=HTTP_400_BAD_REQUEST)
            except ValidationError as e:
                return Response({"error": e}, status=HTTP_400_BAD_REQUEST)
            print(d, u, r)

            serializer2 = StaffMemberSerializer()
            s = serializer2.create(validated_data={"department": d, "role": r, "title": request.data["title"],
                                                   "on_duty": request.data["on_duty"], "member": u})

            return Response(model_to_dict(s))

        except IntegrityError:
            return Response({"error": "The email address is already taken."})


class UpdateProfileView(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UpdateUserSerializer

    def get_object(self):
        if getattr(self, "swagger_fake_view", False):
            return User.objects.none()
        return User.objects.get(pk=self.request.user.id)

    def get_object(self):
        return User.objects.get(pk=self.request.user.id)
    # lookup_field = 'uuid'


class UpdatePasswordView(generics.UpdateAPIView):
    queryset = User.objects.all()
    permission_classes = (IsAuthenticated,)
    serializer_class = ChangePasswordSerializer


class ResetPasswordView(APIView):
    def post(self, request):
        serializer = ResetPasswordSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response({"user": "Password changed successfully."}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ResPassView(APIView):
    @method_decorator(csrf_exempt)
    def post(self, request):
        try:
            user = User.objects.get(email=request.data.get('email'))
            if not user.verified:
                return JsonResponse({"user": "User is not yet verified."}, status=status.HTTP_400_BAD_REQUEST)
            send_token(request.POST, user, reg=False)

            return JsonResponse({"user": "Reset email has been sent."}, status=status.HTTP_200_OK)
        except User.DoesNotExist:
            return JsonResponse({"user": "User does not exist."}, status=status.HTTP_400_BAD_REQUEST)


class IsAuthenticatedView(APIView):
    def get(self, request):
        if request.user.is_authenticated:
            return Response({"authenticated": True})
        return Response({"authenticated": False})


class UserProfileView(ListAPIView):
    permission_classes = (IsUser | IsChef | IsAdmin,)
    serializer_class = UserSerializer

    def get(self, request):
        serializer_class = UserSerializer(instance=User.objects.get(id=request.user.id), many=False)
        return Response({'user_details': serializer_class.data})


class MyUserRolesView(ListAPIView):
    permission_classes = (IsUser | IsChef | IsAdmin | IsWaiter,)
    serializer_class = UserSerializer

    def get(self, request):
        data = request.user.roles.values_list(flat=True)
        serializer_class = RolesSerializer(instance=data, many=False)
        return Response({'roles': serializer_class.instance})
