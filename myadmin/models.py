from django.db import models

from custom_auth.models import User, Role
from utils.models import TimeStampedModel


class Department(TimeStampedModel):
    department_name = models.CharField(max_length=255, unique=True)


class StaffMember(TimeStampedModel):
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    member = models.ForeignKey(User, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    on_duty = models.BooleanField(default=True, null=False)
