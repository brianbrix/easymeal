from django.http import Http404
from rest_framework import generics, status
from rest_framework.generics import CreateAPIView, ListAPIView, UpdateAPIView, DestroyAPIView, RetrieveAPIView, \
    GenericAPIView
from rest_framework.mixins import UpdateModelMixin
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from custom_auth.models import User, Role
from custom_auth.serializers.user_serializers import UserSerializer, RolesSerializer
from hotel.models import Category, MenuItem
from hotel.serializers import CategorySerializer, ItemSerializer
from myadmin.models import Department, StaffMember
from myadmin.serializers import DepartmentSerializer, StaffMemberSerializer
from utils.permissions import IsAdmin, IsChef


class UsersView(ListAPIView):
    queryset = User.objects.filter(verified=True)
    permission_classes = (IsAdmin,)
    serializer_class = UserSerializer


class RolesView(ListAPIView):
    permission_classes = (IsAdmin,)
    serializer_class = RolesSerializer

    def get(self, request):
        queryset = Role.objects.all().values_list(flat=True)
        return Response(queryset)


class CreateStaffMemberView(CreateAPIView):
    permission_classes = (IsAdmin | IsChef,)
    queryset = StaffMember.objects.all()
    serializer_class = StaffMemberSerializer


class StaffMembersView(ListAPIView):
    queryset = StaffMember.objects.all()
    permission_classes = (IsAdmin | IsChef,)
    serializer_class = StaffMemberSerializer


class StaffMemberView(RetrieveAPIView):
    queryset = StaffMember.objects.all()
    permission_classes = (IsAdmin | IsChef,)
    serializer_class = StaffMemberSerializer


class StaffMemberDeleteView(DestroyAPIView):
    queryset = StaffMember.objects.all()
    permission_classes = (IsAdmin | IsChef,)
    serializer_class = StaffMemberSerializer

    def get_object(self, pk):
        try:
            return StaffMember.objects.get(pk=pk)
        except StaffMember.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        cat = self.get_object(pk)
        user = User.objects.get(id=cat.member_id)
        user.roles.remove(cat.role_id)
        cat.delete()
        return Response({"details": "Staff member removed successfully."}, status=status.HTTP_204_NO_CONTENT)


class UpdateStaffMemberView(UpdateAPIView):
    permission_classes = (IsAdmin | IsChef,)
    queryset = StaffMember.objects.all()
    serializer_class = StaffMemberSerializer


class StaffPartialUpdateView(GenericAPIView, UpdateModelMixin):
    '''
    You just need to provide the field which is to be modified.
    '''
    queryset = StaffMember.objects.all()
    serializer_class = StaffMemberSerializer

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class DepartmentsView(ListAPIView):
    queryset = Department.objects.all()
    permission_classes = (IsAdmin,)
    serializer_class = DepartmentSerializer


class DepartmentView(RetrieveAPIView):
    queryset = Department.objects.all()
    permission_classes = (IsAdmin,)
    serializer_class = DepartmentSerializer


class UpdateDepartmentView(UpdateAPIView):
    queryset = Department.objects.all()
    permission_classes = (IsAdmin,)
    serializer_class = DepartmentSerializer


class DeleteDepartmentView(DestroyAPIView):
    queryset = Department.objects.all()
    permission_classes = (IsAdmin,)
    serializer_class = DepartmentSerializer

    def get_object(self, pk):
        try:
            return Department.objects.get(pk=pk)
        except Department.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        cat = self.get_object(pk)
        cat.delete()
        return Response({"details": "Department deleted successfully."}, status=status.HTTP_204_NO_CONTENT)


class CreateDepartmentView(CreateAPIView):
    permission_classes = (IsAdmin,)
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer


class CreateCategoryView(CreateAPIView):
    permission_classes = (IsAdmin | IsChef,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoriesView(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = CategorySerializer

    def get_queryset(self):
        serializer_class = CategorySerializer(context={'request': self.request})
        queryset = Category.objects.all()
        return queryset


class CategoryView(RetrieveAPIView):
    permission_classes = (AllowAny,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    # def get_queryset(self):
    #     serializer_class = CategorySerializer(context={'request': self.request})
    #     queryset = Category.objects.all().order_by('-created')
    #     return queryset


class UpdateCategoryView(UpdateAPIView):
    permission_classes = (IsAdmin | IsChef,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class DeleteCategoryView(APIView):
    permission_classes = (IsAdmin | IsChef,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def get_object(self, pk):
        try:
            return Category.objects.get(pk=pk)
        except Category.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        cat = self.get_object(pk)
        cat.delete()
        return Response({"details": "Category deleted successfully."}, status=status.HTTP_204_NO_CONTENT)


class CreateItemView(CreateAPIView):
    permission_classes = (IsAdmin | IsChef,)
    queryset = MenuItem.objects.all()
    serializer_class = ItemSerializer


class MenuItemsView(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = ItemSerializer

    def get_queryset(self):
        queryset = MenuItem.objects.all()
        return queryset


class ItemView(RetrieveAPIView):
    permission_classes = (AllowAny,)
    queryset = MenuItem.objects.all()
    serializer_class = ItemSerializer

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()  # here the object is retrieved
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class UpdateItemView(UpdateAPIView):
    permission_classes = (IsAdmin | IsChef,)
    queryset = MenuItem.objects.all()
    serializer_class = ItemSerializer


class DeleteItemView(APIView):
    permission_classes = (IsAdmin | IsChef,)
    queryset = MenuItem.objects.all()
    serializer_class = CategorySerializer

    def get_object(self, pk):
        try:
            return MenuItem.objects.get(pk=pk)
        except MenuItem.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        cat = self.get_object(pk)
        cat.delete()
        return Response({"details": "Menu Item removed successfully."}, status=status.HTTP_204_NO_CONTENT)
