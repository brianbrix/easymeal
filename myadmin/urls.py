from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from custom_auth.views import MyObtainTokenPairView, RegisterStaff
from hotel.views import OrderView, OrdersView, UpdateOrderView, VouchersView, LiveOrdersView
from utils.views import get_roles
from .views import *

urlpatterns = [
    path('categories/create/', CreateCategoryView.as_view()),
    path('categories/', CategoriesView.as_view()),
    path('staff/', StaffMembersView.as_view()),
    path('staff/<uuid:pk>/', StaffMemberView.as_view()),
    path('staff/delete/<uuid:pk>/', StaffMemberDeleteView.as_view()),
    path('staff/edit/<uuid:pk>/', UpdateStaffMemberView.as_view()),
    path('staff/switch-duty/<uuid:pk>/', StaffPartialUpdateView.as_view()),
    path('users/', UsersView.as_view()),
    path('staff/add/', RegisterStaff.as_view({"post":"post"})),
    path('departments/', DepartmentsView.as_view()),
    path('departments/<uuid:pk>/', DepartmentView.as_view()),
    path('departments/edit/<uuid:pk>/', UpdateDepartmentView.as_view()),
    path('departments/delete/<uuid:pk>/', DeleteDepartmentView.as_view()),
    path('departments/add/', CreateDepartmentView.as_view()),
    path('categories/<uuid:pk>/', CategoryView.as_view()),
    path('categories/edit/<uuid:pk>/', UpdateCategoryView.as_view()),
    path('categories/delete/<uuid:pk>/', DeleteCategoryView.as_view()),
    path('menu/create/', CreateItemView.as_view()),
    path('menu/items/', MenuItemsView.as_view()),
    path('menu/edit/<uuid:pk>/', UpdateItemView.as_view()),
    path('menu/delete/<uuid:pk>/', DeleteItemView.as_view()),
    path('menu/items/<uuid:pk>/', ItemView.as_view()),
    path('orders/', OrdersView.as_view()),
    path('orders/<int:pk>', OrderView.as_view()),
    path('live-orders/', LiveOrdersView.as_view()),
    path('orders/edit/<int:pk>', UpdateOrderView.as_view()),
    path('vouchers/', VouchersView.as_view()),


]
