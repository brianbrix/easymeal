from django.db.utils import IntegrityError
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from chef.models import Requisition
from chef.serializers import RequisitionSerializer
from custom_auth.models import User, Role
from custom_auth.serializers.user_serializers import UserSerializer
from myadmin.models import Department, StaffMember
from utils.helpers import get_serializer_data


class DepartmentSerializer(serializers.ModelSerializer):
    department_name = serializers.CharField(write_only=False, required=True)

    class Meta:
        model = Department
        fields = '__all__'

    def create(self, validated_data):
        """
        Create and return a new `Category` instance, given the validated data.
        """
        try:
            return Department.objects.create(**validated_data)
        except IntegrityError:
            raise serializers.ValidationError({"error": "Duplicate entry for department_name."})


class StaffMemberSerializer(serializers.ModelSerializer):
    department = serializers.PrimaryKeyRelatedField(queryset=Department.objects.all(), write_only=False, required=True)
    member = serializers.PrimaryKeyRelatedField(queryset=User.objects.filter(verified=True), write_only=False,
                                                required=False)
    role = serializers.PrimaryKeyRelatedField(queryset=Role.objects.all())
    on_duty = serializers.BooleanField(write_only=False, required=True)
    member_details = serializers.SerializerMethodField('get_member')
    title = serializers.CharField(write_only=False, required=True)
    department_details = serializers.SerializerMethodField('get_department')
    requisitions = serializers.SerializerMethodField('get_requisitions')

    class Meta:
        model = StaffMember
        fields = '__all__'

    def get_member(self, staff):
        return get_serializer_data(User, staff.member, UserSerializer)

    def get_department(self, staff):
        return get_serializer_data(Department, staff.department, DepartmentSerializer)

    def get_requisitions(self, staff):
        if "requisitions" in self.context:
            if self.context["requisitions"]:
                if staff.role.role in ["CHEF", "ADMIN"]:
                    try:
                        r = Requisition.objects.filter(user=staff.member).order_by('-created')
                        ser = RequisitionSerializer(instance=r, many=True)
                        return ser.data
                    except Requisition.DoesNotExist:
                        return None

    def update(self, instance, validated_data):
        # Validated member cannot be edited
        u = User.objects.get(id=instance.member.id)
        if not u.verified:
            raise ValidationError({"error": "The user is not yet verified."})
        if 'member' in validated_data:
            validated_data.pop('member')  # validated_data no longer has member
        u.roles.add(validated_data["role"])
        return super().update(instance, validated_data)

    def create(self, validated_data):
        """
        Create and return a new `StaffMember` instance, given the validated data.
        """
        # try:
        f = StaffMember.objects.filter(department=validated_data["department"]).filter(
            member=validated_data["member"]).filter(
            role=validated_data["role"])
        # user = User.objects.get(id=validated_data['member'].id)
        # print(user.roles.all(), user.id)
        if f:
            raise serializers.ValidationError({"error": "The user is already a member of this department."})
        validated_data["on_duty"] = True
        s = StaffMember.objects.create(**validated_data)
        user = User.objects.get(id=validated_data['member'].id)
        # print(user.roles.all())
        user.roles.add(validated_data["role"])
        user.save()
        return s
        # except IntegrityError:
        #     raise serializers.ValidationError({"error": "Duplicate entry for category_name."})
