from django.db.models import QuerySet
from django.db.utils import IntegrityError
from rest_framework import serializers

from chef.models import ChefsCombo
from custom_auth.models import User
from custom_auth.serializers.user_serializers import UserSerializer
from foodicious import settings
from foodicious.settings import MEDIA_URL
from hotel.models import Category, MenuItem, Order, ORDER_STATUS, OrderItem, Voucher
from utils.helpers import get_serializer_data


class CategorySerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(write_only=False, required=True)
    category_items = serializers.SerializerMethodField('get_cat_items')

    def get_cat_items(self, cat):
        u = MenuItem.objects.filter(category=cat)
        data = []
        if len(u) >= 1:
            serializer = ItemSerializer(instance=u, many=True, context=self.context)
            data = list(dict(x) for x in serializer.data)
            if not serializer.data:
                return
        return data

    class Meta:
        model = Category
        fields = '__all__'
        ref_name = "hotel_cat"

    def create(self, validated_data):
        """
        Create and return a new `Category` instance, given the validated data.
        """
        try:
            return Category.objects.create(**validated_data)
        except IntegrityError:
            raise serializers.ValidationError({"error": "Duplicate entry for category_name."})

    def update(self, instance, validated_data):
        """
        Update and return an existing `Category` instance, given the validated data.
        """

        instance.category_name = validated_data.get('category_name', instance.category_name)

        instance.save()
        return instance


class ItemSerializer(serializers.ModelSerializer):
    category = serializers.PrimaryKeyRelatedField(queryset=Category.objects.all(), required=True
                                                  )
    name = serializers.CharField(required=True, write_only=False)
    description = serializers.CharField(required=False, write_only=False)
    initial_price = serializers.DecimalField(required=True, write_only=False, decimal_places=2, max_digits=33)
    new_price = serializers.DecimalField(required=False, write_only=False, decimal_places=2, max_digits=33)
    image = serializers.ImageField(write_only=True, required=True)
    item_image = serializers.SerializerMethodField('get_image', read_only=True)

    category_name = serializers.SerializerMethodField('get_category')

    def get_category(self, item):
        return item.category.category_name

    def get_image(self, instance):
        request = self.context.get('request')
        return request.build_absolute_uri(instance.image.url)
        # return instance.image.url

    class Meta:
        model = MenuItem
        fields = '__all__'
        ref_name = "hotel_item"

    def create(self, validated_data):
        """
        Create and return a new `MenuItem` instance, given the validated data.
        """
        validated_data["new_price"] = validated_data["initial_price"]
        return MenuItem.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `MenuItem` instance, given the validated data.
        """
        if "category" in validated_data:
            instance.category = validated_data.pop('category')
        if "name" in validated_data:
            instance.name = validated_data.get('name', instance.name)
        if "new_price" in validated_data:
            instance.new_price = validated_data.get('new_price', instance.new_price)
        if "image" in validated_data:
            instance.image = validated_data.get('image', instance.image)

        instance.save()
        return instance


class OrderItemSerializer(serializers.ModelSerializer):
    item = serializers.PrimaryKeyRelatedField(queryset=MenuItem.objects.all(), write_only=True, required=True)
    quantity = serializers.IntegerField(write_only=False, required=True)
    order = serializers.PrimaryKeyRelatedField(queryset=Order.objects.all(), write_only=True, required=False)
    combo = serializers.PrimaryKeyRelatedField(queryset=ChefsCombo.objects.filter(active=True), write_only=True,
                                               required=False)
    total_cost = serializers.DecimalField(write_only=False, required=True, max_digits=33, decimal_places=2)
    item_details = serializers.SerializerMethodField('get_menu')

    class Meta:
        model = OrderItem
        fields = '__all__'

    def get_menu(self, it):
        if isinstance(it, OrderItem):
            d = MenuItem.objects.filter(pk=it.item.id)
        else:
            d = MenuItem.objects.filter(pk=str(it['item_id']))
        ser = ItemSerializer(instance=d, many=True, context=self.context)
        if not ser.data:
            return
        return ser.data[0]


class OrderSerializer(serializers.ModelSerializer):
    order_number = serializers.CharField(read_only=True)
    status = serializers.ChoiceField(choices=ORDER_STATUS, write_only=False, required=True)
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), write_only=True, required=False)
    total_cost = serializers.DecimalField(write_only=False, required=True, max_digits=33, decimal_places=2)
    original_total_amount = serializers.DecimalField(write_only=False, required=True, max_digits=33, decimal_places=2)
    voucher_amount = serializers.FloatField(write_only=False)
    paid_amount = serializers.DecimalField(write_only=False, max_digits=33, decimal_places=2)
    chefs_combo = serializers.BooleanField(write_only=False)
    user_details = serializers.SerializerMethodField('get_order_user')
    order_items = serializers.SerializerMethodField('get_order_items')
    phone_number = serializers.CharField(write_only=False, required=True)
    signature = serializers.CharField(write_only=False)

    def get_order_user(self, ord):
        return get_serializer_data(User, ord.user, UserSerializer)

    def get_order_items(self, ord):
        u = OrderItem.objects.filter(order=ord)
        serializer = OrderItemSerializer(instance=u, many=True, context=self.context)
        data = list(dict(x) for x in serializer.data)
        if not serializer.data:
            return
        return data

    class Meta:
        model = Order
        fields = '__all__'


# class VoucherOrderSerializer(OrderSerializer):
#     voucher = serializers.CharField(required=True, write_only=False)


class ComboOrderSerializer(serializers.ModelSerializer):
    combo = serializers.PrimaryKeyRelatedField(queryset=ChefsCombo.objects.filter(active=True), write_only=False,
                                               required=True)
    user_details = serializers.SerializerMethodField('get_order_user')
    order_items = serializers.SerializerMethodField('get_order_items')

    def get_order_user(self, ord):
        return get_serializer_data(User, ord.user, UserSerializer)

    def get_order_items(self, ord):
        u = OrderItem.objects.filter(order=ord).values()
        serializer = OrderItemSerializer(instance=u, many=True)
        data = list(dict(x) for x in serializer.data)
        if not serializer.data:
            return
        return data

    class Meta:
        model = Order
        fields = '__all__'


class VoucherSerializer(serializers.Serializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False)
    voucher_code = serializers.CharField(max_length=255, read_only=True)
    amount_for_each = serializers.FloatField(write_only=False, required=True)
    number_of_users = serializers.IntegerField(write_only=False, required=True)
    total_amount = serializers.IntegerField(write_only=False, required=False)

    def create(self, validated_data):
        return Voucher.objects.create(user=self.context.get("request", None).user,
                                      amount_for_each=validated_data["amount_for_each"]
                                      , number_of_users=validated_data["number_of_users"],
                                      total_amount=int(validated_data["number_of_users"]) * float(
                                          validated_data["amount_for_each"]))
