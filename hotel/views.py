import ast
import datetime
import json
import re

import cryptography
import django
from django.db import transaction
from django.db.models import Q
from django.forms import model_to_dict
from django.http import HttpResponse

from django.shortcuts import get_object_or_404

# Create your views here.
from rest_framework import status
from rest_framework.decorators import permission_classes, api_view, action
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from chef.models import ChefsCombo
from custom_auth.models import User
from foodicious.settings import phone_pattern
from hotel.models import OrderItem, Order, MenuItem, Voucher, VoucherUser
from hotel.serializers import OrderItemSerializer, OrderSerializer, ComboOrderSerializer, ItemSerializer, \
    VoucherSerializer
from utils.c import encrypt, decrypt_message
from utils.helpers import roles_available
from utils.permissions import *


@api_view(['POST'])
def encrypt_external(request):
    received_json_data = json.loads(request.body.decode("utf-8"))
    if not received_json_data or "order" not in received_json_data:
        return Response({"message": "order not in request body"})
    print(received_json_data)
    ms = encrypt(received_json_data["order"]).decode()
    # print(ms)
    return Response({"encrypted_order": ms})


@api_view(['POST'])
def decrypt_external(request):
    received_json_data = json.loads(request.body.decode("utf-8"))
    if not received_json_data or "encrypted_order" not in received_json_data:
        return Response({"message": "encrypted_order not in request body"})
    ms = decrypt_message(received_json_data["encrypted_order"].encode()).decode()
    return Response({"decrypted_order": ms})


class SearchOrderView(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = OrderSerializer

    def get_queryset(self):
        try:
            if self.request.GET["search"] == "":
                return Order.objects.all().order_by("-created")
            queryset = Order.objects.filter(Q(order_number__icontains=self.request.GET["search"]) |
                                            Q(status__icontains=self.request.GET["search"]) | Q(
                user__phone_number__icontains=self.request.GET["search"]) | Q(
                user__name__icontains=self.request.GET["search"])).order_by("-created")

            return queryset
        except KeyError:
            return Order.objects.all().order_by("-created")


class CreateOrderView(CreateAPIView):
    queryset = OrderItem.objects.all()
    permission_classes = (IsUser | IsChef,)
    serializer_class = OrderSerializer

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        total = 0

        voucher_amount = 0
        if "items" not in request.data:
            return Response({"details": "No items specified."}, status=status.HTTP_400_BAD_REQUEST)
        if "voucher_code" in request.data:
            try:
                voucher = Voucher.objects.get(voucher_code=request.data["voucher_code"])
                if voucher.number_of_users >= 1:
                    users = list(VoucherUser.objects.filter(voucher=voucher).values_list('user_id', flat=True))
                    if request.user.id in users and not roles_available(request, ["WAITER"]):
                        return Response({"details": "You have already used this voucher."})
                    voucher_amount = voucher.amount_for_each
                else:
                    return Response({"details": "Invalid voucher specified."}, status=status.HTTP_400_BAD_REQUEST)
            except Voucher.DoesNotExist:
                return Response({"details": "Invalid voucher specified."}, status=status.HTTP_400_BAD_REQUEST)
        if "phone_number" not in request.data:
            return Response({"details": "No phone number specified."}, status=status.HTTP_400_BAD_REQUEST)
        if not bool(re.match(phone_pattern, request.data["phone_number"])):
            return Response({"details": "The phone number format is wrong."}, status=status.HTTP_400_BAD_REQUEST)

        for item in request.data["items"]:
            try:
                print(MenuItem.objects.get(id=item["item"]))
                total_cost = MenuItem.objects.get(id=item["item"]).new_price * item["quantity"]
                item["total_cost"] = total_cost
                total += total_cost
            except MenuItem.DoesNotExist:
                return Response({"error": "The item specified does not exist."}, status=status.HTTP_400_BAD_REQUEST)

            # try:
        paid_amount = total - voucher_amount

        if paid_amount <= 0:
            paid_amount = 0
        order = Order(user=request.user, original_total_amount=total,
                      total_cost=total, voucher_amount=voucher_amount,
                      paid_amount=paid_amount, phone_number=request.data["phone_number"])

        order.save()

        if "voucher_code" in request.data:
            voucher.number_of_users -= 1
            voucher.save()
            VoucherUser.objects.create(user=request.user, voucher=voucher)

        for item in request.data["items"]:
            item["order"] = order.id
        serializer = OrderItemSerializer(data=request.data["items"], many=True, context={'request': self.request})
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        data = serializer.data
        return Response(
            {"details": "Order created successfully", "order_id": order.id, "order_number": order.order_number,
             "order_total_cost": order.original_total_amount, 'discounted_cost': order.total_cost,
             "voucher_amount": order.voucher_amount,
             "paid_amount": order.paid_amount,
             "order_items": data, "signature": order.signature}, status=status.HTTP_200_OK)
        #     else:
        #         return Response({
        #             "details": "Unable to create order.",
        #             "message": f"{pay.json()['message'] if 'message' in pay.json() else pay.json()['error']}."},
        #             status=status.HTTP_400_BAD_REQUEST)
        # return Response({
        #     "details": f"Unable to create order.Please try again later."}, status=status.HTTP_400_BAD_REQUEST)


class CreateOrderFromCombo(CreateAPIView):
    queryset = OrderItem.objects.all()
    permission_classes = (IsUser | IsChef,)
    serializer_class = ComboOrderSerializer

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        if "combo" not in request.data:
            return Response({"details": "No combo was specified."}, status=status.HTTP_400_BAD_REQUEST)
        voucher_amount = 0
        if "phone_number" not in request.data:
            return Response({"details": "No phone number specified."}, status=status.HTTP_400_BAD_REQUEST)
        if not bool(re.match(phone_pattern, request.data["phone_number"])):
            return Response({"details": "The phone number format is wrong."}, status=status.HTTP_400_BAD_REQUEST)

        if "voucher_code" in request.data:
            try:
                voucher = Voucher.objects.get(voucher_code=request.data["voucher_code"])
                if voucher.number_of_users >= 1:
                    voucher_amount = voucher.amount_for_each
                else:
                    return Response({"details": "Invalid voucher specified."}, status=status.HTTP_400_BAD_REQUEST)
            except Voucher.DoesNotExist:
                return Response({"details": "Invalid voucher specified."}, status=status.HTTP_400_BAD_REQUEST)
        try:
            combo = ChefsCombo.objects.get(pk=request.data["combo"])

            paid_amount = combo.total_amount - voucher_amount
            if paid_amount <= 0:
                paid_amount = 0
            order = Order.objects.create(
                user=request.user,
                original_total_amount=combo.original_total_amount,
                total_cost=combo.total_amount,
                chefs_combo=True,
                voucher_amount=voucher_amount,
                paid_amount=paid_amount,
                phone_number=request.data["phone_number"]
            )

            order.save()
            if "voucher_code" in request.data:
                voucher.number_of_users -= 1
                voucher.save()
                VoucherUser.objects.create(user=request.user, voucher=voucher)

            items = OrderItem.objects.filter(combo=combo)
            items.update(order=order)
            serializer = OrderItemSerializer(instance=items.values(), many=True,
                                             context={'request': self.request})

            data = serializer.data
            return Response(
                {"details": "Order created successfully", "order_id": order.id,
                 "order_number": order.order_number,
                 "order_total_cost": order.original_total_amount, 'discounted_cost': order.total_cost,
                 "order_items": data, "signature": order.signature}, status=status.HTTP_200_OK)


        except ChefsCombo.DoesNotExist:
            return Response({"details": "Invalid combo specified."}, status=status.HTTP_400_BAD_REQUEST)


class OrdersView(ListAPIView):
    permission_classes = (IsAdmin,)
    serializer_class = OrderSerializer

    def get_queryset(self):
        serializer_class = OrderSerializer(context={'request': self.request})
        queryset = Order.objects.all().order_by('-created')
        return queryset


class LiveOrdersView(ListAPIView):
    permission_classes = (IsAdmin | IsWaiter | IsChef,)
    serializer_class = OrderSerializer

    def get_queryset(self):
        serializer_class = OrderSerializer(context={'request': self.request})
        waiters = list(User.objects.filter(roles__in=["WAITER"], verified=True))
        print(waiters)
        queryset = Order.objects.filter(user__in=waiters, status='PENDING').order_by('-created')
        return queryset


class TodaysOrdersView(ListAPIView):
    permission_classes = (IsAdmin | IsWaiter | IsChef,)
    serializer_class = OrderSerializer

    def get_queryset(self):
        serializer_class = OrderSerializer(context={'request': self.request})
        print(django.utils.timezone.now() - datetime.timedelta(days=2), "@@@@@@@")
        queryset = Order.objects.filter(
            created__gte=datetime.datetime.combine(datetime.datetime.now() - datetime.timedelta(days=2),
                                                   datetime.datetime.min.time()),
            created__lte=datetime.datetime.combine(datetime.datetime.now() - datetime.timedelta(days=1),
                                                   datetime.datetime.min.time()),
            status='PENDING').order_by('-created')
        return queryset


class TomorrowsOrdersView(ListAPIView):
    permission_classes = (IsAdmin | IsWaiter | IsChef,)
    serializer_class = OrderSerializer

    def get_queryset(self):
        serializer_class = OrderSerializer(context={'request': self.request})
        queryset = Order.objects.filter(
            created__gte=datetime.datetime.combine(datetime.datetime.now() - datetime.timedelta(days=1),
                                                   datetime.datetime.min.time()),
            created__lte=datetime.datetime.combine(datetime.datetime.now(), datetime.datetime.min.time()),
            status='PENDING').order_by('-created')
        return queryset


class OrderView(RetrieveAPIView):
    queryset = Order.objects.all()
    permission_classes = (IsUser | IsAdmin | IsChef,)
    serializer_class = OrderSerializer

    def get_queryset(self):
        serializer_class = OrderSerializer(context={'request': self.request})
        queryset = Order.objects.all()
        return queryset


class LiveOrdersView(ListAPIView):
    permission_classes = (IsUser | IsAdmin | IsChef | IsWaiter,)
    serializer_class = OrderSerializer

    def get_queryset(self):
        serializer_class = OrderSerializer(context={'request': self.request})
        waiters = User.objects.filter(roles__in=["WAITER"])
        queryset = Order.objects.filter(user__in=waiters)
        return queryset


class MyOrdersView(ListAPIView):
    permission_classes = (IsUser,)
    serializer_class = OrderSerializer

    def get(self, request, *args):
        queryset = Order.objects.filter(user=request.user).order_by('-created')
        serializer = OrderSerializer(queryset, many=True, context={'request': self.request}).data
        return Response({"details": serializer})


class UpdateOrderView(UpdateAPIView):
    queryset = Order.objects.all()
    permission_classes = (IsUser | IsAdmin | IsChef,)
    serializer_class = OrderSerializer

    def put(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        if order.status == "REIMBURSED" or order.status == "CANCELLED":
            return Response({"details": "Order has already been cancelled."})
        self.partial_update(request, *args, **kwargs)
        order.status = "REIMBURSED"
        order.save()

        v = Voucher.objects.create(user=request.user,
                                   amount_for_each=order.total_cost
                                   , number_of_users=1,
                                   total_amount=order.total_cost)
        s = VoucherSerializer(instance=v, many=False)
        return Response({"details": "Order cancelled successfully.", "voucher_details": s.data})

    def patch(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        if order.status == "REIMBURSED" or order.status == "CANCELLED":
            return Response({"details": "Order has already been cancelled."})
        self.partial_update(request, *args, **kwargs)
        v = Voucher.objects.create(user=request.user,
                                   amount_for_each=order.total_cost
                                   , number_of_users=1,
                                   total_amount=order.total_cost)
        order.status = "REIMBURSED"
        order.save()
        s = VoucherSerializer(instance=v, many=False)
        return Response({"details": "Order cancelled successfully.", "voucher_details": s.data})


class TrendingView(ListAPIView):
    permission_classes = (IsUser | IsAdmin | IsChef,)
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer

    def get_queryset(self):
        serializer_class = OrderItemSerializer(context={'request': self.request})
        if getattr(self, "swagger_fake_view", False):
            return Order.objects.none()
        queryset = Order.objects.all()
        # print(queryset)
        return queryset

    def get(self, request):
        last_week = datetime.datetime.now() - datetime.timedelta(days=7)
        items = OrderItem.objects.filter(created__gte=last_week)
        s = OrderItemSerializer(instance=items, many=True, context={'request': self.request})
        ids = [x["item_details"]["id"] for x in s.data if x["item_details"] is not None]
        counts = {x: ids.count(x) for x in ids}
        sorted_counts = [k for k, v in sorted(counts.items(), key=lambda item: item[1], reverse=True)]
        if len(sorted_counts) >= 1:
            sorted_counts = sorted_counts[:5]
        menu_items = MenuItem.objects.filter(pk__in=sorted_counts)
        s = ItemSerializer(instance=menu_items, many=True, context={'request': self.request})
        return Response(s.data)


class CreateVoucherView(CreateAPIView):
    permission_classes = (IsUser | IsAdmin | IsChef,)
    queryset = Voucher.objects.all()
    serializer_class = VoucherSerializer


class VouchersView(ListAPIView):
    permission_classes = (IsAdmin | IsChef,)
    queryset = Voucher.objects.all()
    serializer_class = VoucherSerializer


class UserVouchersView(ListAPIView):
    permission_classes = (IsUser,)
    serializer_class = VoucherSerializer

    def get_queryset(self):
        if getattr(self, "swagger_fake_view", False):
            return Order.objects.none()
        return Voucher.objects.filter(user=self.request.user, number_of_users__gte=1)


class VerifyOrder(APIView):
    permission_classes = (IsWaiter | IsAdmin | IsChef,)
    serializer_class = OrderSerializer

    def post(self, request, *args, **kwargs):
        if "signature" not in request.data:
            return Response({"details": "You must pass the unique id for the order."},
                            status=status.HTTP_400_BAD_REQUEST)
        if "order_id" not in request.data:
            return Response({"details": "You must pass the unique id for the order."},
                            status=status.HTTP_400_BAD_REQUEST)

        try:

            decrypted_order = ast.literal_eval(decrypt_message(request.data["signature"].encode()).decode())
        except cryptography.fernet.InvalidToken:
            return Response({"error": "The order is could not be verified."}, status=status.HTTP_400_BAD_REQUEST)

        queryset = get_object_or_404(Order, pk=request.data["order_id"], signature=request.data["signature"])
        today_last_week = datetime.datetime.now() - datetime.timedelta(days=7)
        if queryset.created < today_last_week:
            return Response({"error": "The order is expired."}, status=status.HTTP_400_BAD_REQUEST)
        d = {str(k): str(v) for k, v in model_to_dict(queryset).items() if str(k) != "signature"}
        if "order_number" in decrypted_order:
            if d["order_number"] == decrypted_order["order_number"]:
                serializer_class = OrderSerializer(instance=queryset, context={'request': self.request})

                return Response({"order": serializer_class.data}, status=status.HTTP_200_OK)
        else:
            return Response({"error": "The order is could not be verified."}, status=status.HTTP_400_BAD_REQUEST)


class AddSig(UpdateAPIView):
    permission_classes = (IsWaiter | IsAdmin | IsChef,)
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    @transaction.atomic
    def update(self, request, *args, **kwargs):
        order = get_object_or_404(Order, pk=kwargs["pk"])
        d = str({str(k): str(v) for k, v in model_to_dict(order).items() if str(k) != "signature"})
        order.signature = encrypt(d).decode()
        order.save()
        return Response({"details": "Signature refreshed."})


class AddSigToAll(ModelViewSet):
    permission_classes = (IsWaiter | IsAdmin | IsChef,)
    serializer_class = OrderSerializer

    @action(detail=True, methods=['post'])
    def post(self, request):
        today_last_week = datetime.datetime.now() - datetime.timedelta(days=7)
        ordrs = Order.objects.filter(created__gt=today_last_week, created__lt=datetime.datetime.now())
        with transaction.atomic():
            for i in ordrs:
                d = str({str(k): str(v) for k, v in model_to_dict(i).items() if str(k) != "signature"})
                i.signature = encrypt(d).decode()
                i.save()
        return Response("Signature refreshed.")
