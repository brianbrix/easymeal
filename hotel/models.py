import string
import uuid
import random

import model_utils
from django.db import models

# Create your models here.
from chef.models import ChefsCombo
from custom_auth.models import UploadToPathAndRename, User
from utils.models import TimeStampedModel


class Category(TimeStampedModel):
    category_name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.category_name


class MenuItem(TimeStampedModel):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255, null=True)
    initial_price = models.DecimalField(null=True, decimal_places=2, max_digits=33)
    new_price = models.DecimalField(null=True, decimal_places=2, max_digits=33)
    image = models.ImageField(upload_to=UploadToPathAndRename('hotel'), null=False)


ORDER_STATUS = (
    ("PENDING", "PENDING"),
    ("PAID", "PAID"),
    ("CANCELLED", "CANCELLED"),
    ("RECEIVED", "RECEIVED"),
    ("REIMBURSED", "REIMBURSED"),
    ("PENDING", "PENDING"),
    ("AWAITING_PROCESSING", "AWAITING_PROCESSING"),
)


class Order(model_utils.models.TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    order_number = models.CharField(max_length=255, unique=True, null=True)
    status = models.CharField(choices=ORDER_STATUS, max_length=255, default="AWAITING_PROCESSING")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user')
    total_cost = models.DecimalField(null=True, decimal_places=2, max_digits=33)
    original_total_amount = models.DecimalField(null=True, decimal_places=2, max_digits=33)
    voucher_amount = models.FloatField(null=True)
    paid_amount = models.DecimalField(null=True, decimal_places=2, max_digits=33)  ##non_voucher amount
    chefs_combo = models.BooleanField(default=False)
    phone_number = models.CharField(max_length=255)
    signature = models.TextField(null=True)

    # def addSignature(self):
    #     self.signature = encrypt(str({"order_number": self.order_number, "status":self.status, "user":self.user, "total_cost":self.total.cost, ""})}))


class OrderNumber(model_utils.models.TimeStampedModel):
    order_number = models.CharField(max_length=255, unique=True)


class OrderItem(TimeStampedModel):
    item = models.ForeignKey(MenuItem, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True)
    combo = models.ForeignKey(ChefsCombo, on_delete=models.CASCADE, null=True)
    total_cost = models.FloatField()


def key_generator():
    key = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(6))
    if Voucher.objects.filter(voucher_code=key).exists():
        key = key_generator()
    return key


class Voucher(TimeStampedModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    voucher_code = models.CharField(max_length=255, default=key_generator, unique=True, editable=False)
    amount_for_each = models.FloatField()
    number_of_users = models.IntegerField()
    total_amount = models.IntegerField()


class VoucherUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    voucher = models.ForeignKey(Voucher, on_delete=models.CASCADE)
