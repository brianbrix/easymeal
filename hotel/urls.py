from django.urls import path

from chef.views import UserCombosView
from myadmin.views import CategoriesView, CategoryView, MenuItemsView, ItemView
from transaction.views import ProcessInvoiceView, ListMyInvoices, ListAllInvoices
from .views import *

urlpatterns = [
    path('categories/', CategoriesView.as_view()),
    path('categories/<uuid:pk>/', CategoryView.as_view()),
    path('menu/items/', MenuItemsView.as_view()),
    path('menu/items/<uuid:pk>/', ItemView.as_view()),
    path('orders/create/', CreateOrderView.as_view()),
    path('orders/edit/<uuid:pk>/', UpdateOrderView.as_view()),
    path('orders/<uuid:pk>', OrderView.as_view()),
    path('orders/verify/', VerifyOrder.as_view()),
    path('my-orders/', MyOrdersView.as_view()),
    path('orders/create/', CreateOrderView.as_view()),
    path('orders/create-from-combo/', CreateOrderFromCombo.as_view()),
    path('orders/', OrdersView.as_view()),
    path('search', SearchOrderView.as_view()),
    path('combos/', UserCombosView.as_view()),
    path('trending/', TrendingView.as_view()),
    path('vouchers/create/', CreateVoucherView.as_view()),
    path('my-vouchers/', UserVouchersView.as_view()),
    path('orders/ref-sig/<uuid:pk>/', AddSig.as_view()),
    path('orders/ref-sig/all/', AddSigToAll.as_view({"post": "post"})),
    path('invoice/pay/<uuid:pk>/', ProcessInvoiceView.as_view()),
    path('my-invoices/', ListMyInvoices.as_view()),
    path('all-invoices/', ListAllInvoices.as_view()),
    path('encrypt/', encrypt_external),
    path('decrypt/', decrypt_external)

]
