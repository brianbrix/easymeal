from django.db import IntegrityError, transaction
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from chef.models import ChefsCombo, Requisition
from custom_auth.models import User
from custom_auth.serializers.user_serializers import UserSerializer
from hotel.models import OrderItem
from hotel.serializers import OrderItemSerializer
from store.models import RequisitionUpdate
from utils.helpers import get_serializer_data, custom_validation_for_fields


class ChefsComboSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    total_amount = serializers.FloatField()
    original_total_amount = serializers.FloatField()
    name = serializers.CharField(required=True, write_only=False)
    image = serializers.ImageField(write_only=False, required=True)
    active = serializers.BooleanField(write_only=False)
    user_details = serializers.SerializerMethodField('get_order_user')
    combo_items = serializers.SerializerMethodField('get_order_items')

    def get_order_user(self, ord):
        return get_serializer_data(User, ord.user, UserSerializer)

    def get_order_items(self, ord):
        u = OrderItem.objects.filter(combo=ord).values()
        serializer = OrderItemSerializer(instance=u, many=True, context=self.context)
        data = list(dict(x) for x in serializer.data)
        if not serializer.data:
            return
        return data

    class Meta:
        model = ChefsCombo
        fields = '__all__'


class RequisitionSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False)
    item_name = serializers.CharField(required=True, write_only=False)
    quantity_requested = serializers.FloatField(required=False, write_only=False)
    # quantity_given = serializers.FloatField(required=False, write_only=False)
    units = serializers.CharField(required=False, write_only=False)
    # staff_details = serializers.SerializerMethodField('get_user')
    requisition_history = serializers.SerializerMethodField('get_history')

    class Meta:
        model = Requisition
        fields = '__all__'

    # def get_user(self, ord):
    #     # print(ord.user.id)
    #     try:
    #         s = StaffMember.objects.get(member=ord.user.id)
    #         ser = StaffMemberSerializer(instance=s, many=False)
    #         if not ser.data:
    #             return
    #         return ser.data
    #     except StaffMember.DoesNotExist:
    #         return {}
    # return get_serializer_data(User, ord.user, UserSerializer)

    def get_history(self, req):
        return RequisitionUpdate.objects.filter(requisition=req).order_by('-created').values()

    @transaction.atomic
    def create(self, validated_data):
        fields = ["quantity_requested", "units"]
        custom_validation_for_fields(fields, validated_data)
        try:

            r = Requisition.objects.create(user=self.context.get("request", None).user,
                                           item_name=validated_data["item_name"])
            try:
                RequisitionUpdate.objects.create(requisition_id=r.id,
                                                 quantity_requested=validated_data["quantity_requested"],
                                                 quantity_remaining=validated_data["quantity_requested"],
                                                 units=validated_data["units"])
            except IntegrityError as e:
                raise ValidationError({"error": str(e)})
            except TypeError as e:
                raise ValidationError({"error": str(e)})
            return r

        except IntegrityError:
            req = Requisition.objects.get(item_name=validated_data["item_name"])
            raise ValidationError({"details": "It seems you requested another item with the same name. Please edit "
                                              "the item requisition instead of creating a new one.",
                                   'requisition_id': req.id})

    @transaction.atomic
    def update(self, instance, validated_data):
        try:
            if "quantity_requested" in validated_data:
                RequisitionUpdate.objects.create(requisition_id=instance.id,
                                                 quantity_requested=validated_data["quantity_requested"],
                                                 quantity_remaining=validated_data["quantity_requested"],
                                                 units=validated_data["units"])
                # instance.current_quantity += validated_data["quantity_requested"]
                instance.quantity_requested = validated_data["quantity_requested"]
            # if "quantity_given" in validated_data:
            #     RequisitionUpdate.objects.create(requisition_id=instance.id,
            #                                      quantity_given=validated_data["quantity_given"], units=validated_data["units"])
            # instance.current_quantity -= validated_data["quantity_given"]
            # instance.quantity_given = validated_data["quantity_given"]
            if "units" in validated_data:
                instance.units = validated_data["units"]
            if "name" in validated_data:
                instance.name = validated_data["name"]
            instance.save()
            return instance
        except IntegrityError as e:
            raise ValidationError({"error": str(e)})


class RequisitionUpdateSerializer(serializers.ModelSerializer):
    requisition = serializers.PrimaryKeyRelatedField(queryset=Requisition.objects.all(), write_only=False)
    quantity_requested = serializers.FloatField(required=True, write_only=False)
    quantity_given_last = serializers.FloatField(required=False, write_only=False)
    quantity_remaining = serializers.FloatField(required=False, write_only=False)
    status = serializers.ChoiceField(
        choices=[("PENDING", "PENDING"), ("PROCESSED", "PROCESSED"), ("CANCELLED", "CANCELLED"),
                 ("INCOMPLETE", "INCOMPLETE")], write_only=False,
        required=True)

    class Meta:
        model = RequisitionUpdate
        fields = '__all__'

    @transaction.atomic
    def update(self, instance, validated_data):
        fields = ["quantity_given_last"]
        custom_validation_for_fields(fields, validated_data)
        if "quantity_given_last" in validated_data:
            if validated_data["quantity_given_last"] > instance.quantity_remaining:
                raise ValidationError({"quatity_remaining":instance.quantity_remaining, "details": "You cannot issue more than the remaining quantity."})
            if validated_data["quantity_given_last"] < instance.quantity_remaining:
                instance.status = "INCOMPLETE"
            if validated_data["quantity_given_last"] == instance.quantity_remaining:
                instance.status = "PROCESSED"
        instance.quantity_remaining -= validated_data["quantity_given_last"]
        instance.quantity_given_last = validated_data["quantity_given_last"]
        instance.save()
        return instance
