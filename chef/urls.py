from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from custom_auth.views import MyObtainTokenPairView
from hotel.views import VouchersView
from .views import *

urlpatterns = [
    path('combos/create/', CreateComboView.as_view()),
    path('requisitions/create/', CreateRequisitionView.as_view()),
    path('requisitions/update/<uuid:pk>/', UpdateRequisitionView.as_view()),
    path('requisitions/history/', ViewRequisitionHistory.as_view()),
    path('combos/', CombosView.as_view()),
    path('combos/edit/<uuid:pk>/', UpdateComboView.as_view()),
    path('combos/remove/<uuid:pk>/', RemoveComboView.as_view()),
    path('combos/delete/<uuid:pk>/', DeleteComboView.as_view()),
    path('combos/<uuid:pk>/', ComboView.as_view()),
    path('vouchers/', VouchersView.as_view()),

]
