from django.http import Http404
from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.generics import CreateAPIView, ListAPIView, UpdateAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet, ModelViewSet

from chef.models import ChefsCombo, Requisition
from chef.serializers import ChefsComboSerializer, RequisitionSerializer, RequisitionUpdateSerializer
from foodicious.settings import USER_ROLES
from hotel.serializers import OrderItemSerializer
from store.models import RequisitionUpdate
from utils.permissions import IsAdmin, IsChef, IsUser


class CreateComboView(CreateAPIView):
    queryset = ChefsCombo.objects.all()
    permission_classes = (IsAdmin | IsChef,)
    serializer_class = ChefsComboSerializer

    def create(self, request, *args, **kwargs):
        total = 0
        for item in request.data["items"]:
            total += item["total_cost"]
        # try:
        if "total_cost" not in request.data:
            return Response({"details": "The field 'total_amount' is required."},
                            status=status.HTTP_400_BAD_REQUEST)
        combo = ChefsCombo.objects.create(
            user=request.user,
            original_total_amount=total,
            total_amount=request.data["total_cost"],
        )

        for item in request.data["items"]:
            item["combo"] = combo.id
        serializer = OrderItemSerializer(data=request.data["items"], many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        data = serializer.data

        return Response(
            {"details": "Combo created successfully.", "combo_id": combo.id,
             "combo_items": data}, status=status.HTTP_200_OK)


class CombosView(ListAPIView):
    queryset = ChefsCombo.objects.all()
    permission_classes = (IsAdmin | IsChef,)
    serializer_class = ChefsComboSerializer

    def get_queryset(self):
        serializer_class = ChefsComboSerializer(context={'request': self.request})
        queryset = ChefsCombo.objects.all()
        return queryset


class UserCombosView(ListAPIView):
    queryset = ChefsCombo.objects.filter(active=True)
    permission_classes = (IsUser,)
    serializer_class = ChefsComboSerializer


class RemoveComboView(UpdateAPIView):
    permission_classes = (IsAdmin,)
    queryset = ChefsCombo.objects.all()
    serializer_class = ChefsComboSerializer

    def get_object(self, pk):
        try:
            return ChefsCombo.objects.get(pk=pk)
        except ChefsCombo.DoesNotExist:
            raise Http404

    def partial_update(self, request, pk, *args, **kwargs):
        comb = self.get_object(pk)
        if not comb.active:
            return Response({"details": "The combo is not active."}, status=status.HTTP_400_BAD_REQUEST)
        s = ChefsComboSerializer(instance=comb, many=False)
        s.update(comb, {"active": False})
        return Response({"details": s.data}, status=status.HTTP_204_NO_CONTENT)


class DeleteComboView(APIView):
    permission_classes = (IsAdmin,)
    queryset = ChefsCombo.objects.all()
    serializer_class = ChefsComboSerializer

    def get_object(self, pk):
        try:
            return ChefsCombo.objects.get(pk=pk)
        except ChefsCombo.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        comb = self.get_object(pk)
        comb.delete()
        return Response({"details": "Combo removed successfully."}, status=status.HTTP_204_NO_CONTENT)


class UpdateComboView(UpdateAPIView):
    queryset = ChefsCombo.objects.all()
    permission_classes = (IsAdmin | IsChef,)
    serializer_class = ChefsComboSerializer

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)


class ComboView(RetrieveAPIView):
    queryset = ChefsCombo.objects.all()
    permission_classes = (IsAdmin | IsChef,)
    serializer_class = ChefsComboSerializer


class CreateRequisitionView(CreateAPIView):
    queryset = Requisition.objects.all()
    permission_classes = (IsAdmin | IsChef,)
    serializer_class = RequisitionSerializer


class UpdateRequisitionView(UpdateAPIView):
    queryset = Requisition.objects.all()
    permission_classes = (IsAdmin | IsChef,)
    serializer_class = RequisitionSerializer


class ViewRequisitionHistory(ListAPIView):
    permission_classes = (IsAdmin | IsChef,)
    serializer_class = RequisitionUpdateSerializer

    def get_queryset(self):
        if "requisition_id" not in self.request.data:
            return Response({"details": "requisition_id must be specified."}, status=status.HTTP_400_BAD_REQUEST)
        return RequisitionUpdate.objects.filter(requisition_id=self.request.data["requisition_id"])
