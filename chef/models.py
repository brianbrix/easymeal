import django
from django.db import models

# Create your models here.
from custom_auth.models import User
from utils.models import TimeStampedModel, UploadToPathAndRename


class ChefsCombo(TimeStampedModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='combo_user')
    total_amount = models.FloatField()
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to=UploadToPathAndRename('hotel'), null=False)
    original_total_amount = models.FloatField()
    active = models.BooleanField(default=True)


class Requisition(TimeStampedModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item_name = models.CharField(max_length=255, unique=True)
    # quantity_requested = models.FloatField(max_length=255, null=True)
    # quantity_given = models.FloatField(max_length=255, null=True)
    # units = models.CharField(max_length=255)


# class RequisitionUpdate(models.Model):
#     user = models.ForeignKey(User, on_delete=models.CASCADE)

