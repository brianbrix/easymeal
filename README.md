# Easy meal Documentation

## Introduction
Restaurant app


## Installation
1.  GIT 
    
    Clone the repository first
    ```
    $ git clone <repo url>
    ```
    
2.  DOTENV  & ENVIRONMENT VARIABLES
    - Creating the file
    ```
    $ cd foodapp

    $ touch .env
    ```
    - Sample .env 
        ```
        SECRET_KEY=
        DEBUG=
        DJANGO_ALLOWED_HOSTS=
        DATABASE_NAME=
        DATABASE_USER=
        DATABASE_PASSWORD=
        DATABASE_HOST=
        DATABASE_PORT=
        EMAIL_BACKEND=
        EMAIL_HOST=
        EMAIL_HOST_USER=
        EMAIL_HOST_PASSWORD=
        EMAIL_PORT=
        DEFAULT_FROM_EMAIL=

        ```
    ```
3. DOCKER CONFIGURATION
    
    - Dockerfile
        - Contains the commands to assemble the image.
    
    - Docker Compose
        - Contains the .yml files for defining the applications / containers.

    - Deploying & Building the Application 

        The steps are : Building and Serve the Application
        ```
        Building the application - Builds a new image. 
        ```
        $ docker build -t foodapp_backend .
        ```
        or 
        Building the application - Skipping the image build process.
        ```
        $ docker build -t foodapp_backend . 
        ```
        - Running the Application
        ```
        $ docker-compose up -d
        ```
        - Powering down the application
        ```
        $  docker-compose down
            ```

## OTHER MANAGEMENT COMMANDS