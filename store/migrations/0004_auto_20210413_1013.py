# Generated by Django 3.1.5 on 2021-04-13 07:13

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0003_auto_20210413_1007'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requisitionupdate',
            name='created_on',
            field=models.DateTimeField(default=datetime.datetime(2021, 4, 13, 7, 13, 38, 906460, tzinfo=utc)),
        ),
    ]
