from django.http import Http404
from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from chef.models import Requisition
from chef.serializers import RequisitionSerializer, RequisitionUpdateSerializer
from custom_auth.models import User
from myadmin.models import StaffMember
from myadmin.serializers import StaffMemberSerializer
from store.models import Category, InventoryItem, RequisitionUpdate
from store.serializers import CategorySerializer, ItemSerializer
from utils.permissions import IsAdmin, IsChef, IsStoreKeeper


class CreateCategoryView(CreateAPIView):
    permission_classes = (IsAdmin | IsStoreKeeper,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoriesView(ListAPIView):
    permission_classes = (IsAdmin | IsStoreKeeper,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class RequisitionsView(ListAPIView):
    permission_classes = (IsAdmin | IsStoreKeeper | IsChef,)
    queryset = StaffMember.objects.all()
    serializer_class = StaffMemberSerializer

    def get_serializer_context(self):
        context = super(RequisitionsView, self).get_serializer_context()
        context.update({"requisitions": True})
        return context


class RequisitionDetailsView(RetrieveAPIView):
    permission_classes = (IsAdmin | IsStoreKeeper,)
    queryset = Requisition.objects.all()
    serializer_class = RequisitionSerializer

    # def get(self):
    #     users =


class StaffRequisitionsView(ListAPIView):
    permission_classes = (IsAdmin | IsStoreKeeper,)
    serializer_class = RequisitionSerializer

    def get_queryset(self):
        queryset = None
        filter_value = self.request.query_params.get('user_id', None)
        # if filter_value is None:
        #     return Response({"una"})

        if filter_value is not None:
            queryset = Requisition.objects.filter(user=User.objects.get(id=filter_value))
        # ser = RequisitionSerializer(instance=queryset, many=True)
        return queryset


# class RequisitionUpdate(UpdateAPIView):
#     permission_classes = (IsAdmin | IsStoreKeeper,)
#     queryset = RequisitionUpdate.objects.all()
#     serializer_class = RequisitionUpdateSerializer

class RequisitionUpdateView(UpdateAPIView):
    permission_classes = (IsAdmin | IsStoreKeeper,)
    queryset = RequisitionUpdate.objects.all()
    serializer_class = RequisitionUpdateSerializer


class CategoryView(RetrieveAPIView):
    permission_classes = (IsAdmin | IsStoreKeeper,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class UpdateCategoryView(UpdateAPIView):
    permission_classes = (IsAdmin | IsStoreKeeper,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class DeleteCategoryView(APIView):
    permission_classes = (IsAdmin | IsStoreKeeper,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def get_object(self, pk):
        try:
            return Category.objects.get(pk=pk)
        except Category.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        cat = self.get_object(pk)
        cat.delete()
        return Response({"details": "Category deleted successfully."}, status=status.HTTP_204_NO_CONTENT)


class CreateItemView(CreateAPIView):
    permission_classes = (IsAdmin | IsStoreKeeper,)
    queryset = InventoryItem.objects.all()
    serializer_class = ItemSerializer


class InventoryItemsView(ListAPIView):
    permission_classes = (IsAdmin | IsStoreKeeper,)
    queryset = InventoryItem.objects.all()
    serializer_class = ItemSerializer


class ItemView(RetrieveAPIView):
    permission_classes = (IsAdmin | IsStoreKeeper,)
    queryset = InventoryItem.objects.all()
    serializer_class = ItemSerializer

    # def get(self, request):
    #     serializer_class = ItemSerializer(context={"request": request})
    #     return serializer_class.data


class UpdateItemView(UpdateAPIView):
    permission_classes = (IsAdmin | IsStoreKeeper,)
    queryset = InventoryItem.objects.all()
    serializer_class = ItemSerializer


class DeleteItemView(APIView):
    permission_classes = (IsAdmin | IsStoreKeeper,)
    queryset = InventoryItem.objects.all()
    serializer_class = CategorySerializer

    def get_object(self, pk):
        try:
            return InventoryItem.objects.get(pk=pk)
        except InventoryItem.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        cat = self.get_object(pk)
        cat.delete()
        return Response({"details": "Inventory Item removed successfully."}, status=status.HTTP_204_NO_CONTENT)
