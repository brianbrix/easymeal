from django.db import IntegrityError
from rest_framework import serializers

from store.models import InventoryItem, Category


class CategorySerializer(serializers.ModelSerializer):
    category_name = serializers.CharField(write_only=False, required=True)
    category_items = serializers.SerializerMethodField('get_cat_items')

    def get_cat_items(self, cat):
        u = InventoryItem.objects.filter(category_id=cat.id)
        data = []
        if len(u) >= 1:
            serializer = ItemSerializer(instance=u, many=True, context=self.context)
            data = list(dict(x) for x in serializer.data)
        return data

    class Meta:
        model = Category
        fields = '__all__'
        ref_name = "store_cat"

    def create(self, validated_data):
        """
        Create and return a new `Category` instance, given the validated data.
        """
        try:
            return Category.objects.create(**validated_data)
        except IntegrityError:
            raise serializers.ValidationError({"error": "Duplicate entry for category_name."})

    def update(self, instance, validated_data):
        """
        Update and return an existing `Category` instance, given the validated data.
        """

        instance.category_name = validated_data.get('category_name', instance.category_name)

        instance.save()
        return instance


class ItemSerializer(serializers.ModelSerializer):
    category = serializers.PrimaryKeyRelatedField(queryset=Category.objects.all(), required=True, many=False
                                                  )
    name = serializers.CharField(required=True, write_only=False)
    quantity = serializers.FloatField(required=True, write_only=False)
    description = serializers.CharField(write_only=False, required=False)
    units = serializers.CharField(write_only=False, required=True)
    image = serializers.ImageField(write_only=False, required=True)
    price_per_item = serializers.DecimalField(write_only=False, required=True, max_digits=33, decimal_places=2)
    item_image = serializers.SerializerMethodField('get_image')

    category_name = serializers.SerializerMethodField('get_category')

    def get_category(self, item):
        return item.category.category_name

    def get_image(self, instance):
        request = self.context.get('request')
        return request.build_absolute_uri(instance.image.url)

    class Meta:
        model = InventoryItem
        fields = '__all__'
        ref_name = "store_item"

    def create(self, validated_data):
        """
        Create and return a new `MenuItem` instance, given the validated data.
        """
        return InventoryItem.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `MenuItem` instance, given the validated data.
        """
        if "category" in validated_data:
            instance.category = validated_data.pop('category')
        if "name" in validated_data:
            instance.name = validated_data.get('name', instance.name)
        if "quantity" in validated_data:
            instance.quantity = validated_data.get('quantity', instance.quantity)
        if "image" in validated_data:
            instance.image = validated_data.get('image', instance.image)
        if "units" in validated_data:
            instance.units = validated_data.get('units', instance.units)

        instance.save()
        return instance

