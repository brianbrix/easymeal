import uuid

import django
import model_utils.models
from django.db import models

# Create your models here.
from chef.models import Requisition
from utils.models import TimeStampedModel, UploadToPathAndRename


class Category(TimeStampedModel):
    category_name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.category_name


class InventoryItem(TimeStampedModel):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255, null=True)
    quantity = models.FloatField(max_length=60)
    units = models.CharField(max_length=255)
    image = models.ImageField(upload_to=UploadToPathAndRename('store'), null=False)
    price_per_item = models.DecimalField(max_digits=33, decimal_places=2)


class Inventory(TimeStampedModel):
    name = models.CharField(max_length=255)


class RequisitionUpdate(model_utils.models.TimeStampedModel):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    requisition = models.ForeignKey(Requisition, on_delete=models.CASCADE)
    # created_on = models.DateTimeField(default=django.utils.timezone.now())
    quantity_requested = models.FloatField(max_length=255, null=True)
    quantity_given_last = models.FloatField(max_length=255, null=True, default=0)
    quantity_remaining = models.FloatField(max_length=255, null=True)
    units = models.CharField(max_length=255)
    status = models.CharField(choices=[("PENDING", "PENDING"), ("PROCESSED", "PROCESSED"), ("CANCELLED", "CANCELLED"), ("INCOMPLETE", "INCOMPLETE")],
                              default="PENDING", max_length=20)


class RequisitionIssuance(TimeStampedModel):
    requisition_record = models.ForeignKey(RequisitionUpdate, on_delete=models.CASCADE)
    quantity_given = models.FloatField(null=True)
    quantity_remaining = models.FloatField(null=True)
