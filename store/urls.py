from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from custom_auth.views import MyObtainTokenPairView
from hotel.views import VouchersView
from .views import *

urlpatterns = [
    path('inventory/categories/create/', CreateCategoryView.as_view()),
    path('inventory/categories/', CategoriesView.as_view()),
    path('inventory/categories/<uuid:pk>/', CategoryView.as_view()),
    path('inventory/categories/edit/<uuid:pk>/', UpdateCategoryView.as_view()),
    path('inventory/categories/delete/<uuid:pk>/', DeleteCategoryView.as_view()),
    path('inventory/items/create/', CreateItemView.as_view()),
    path('inventory/items/', InventoryItemsView.as_view()),
    path('inventory/items/<uuid:pk>/', ItemView.as_view()),
    path('inventory/items/edit/<uuid:pk>/', UpdateItemView.as_view()),
    path('inventory/items/delete/<uuid:pk>/', DeleteItemView.as_view()),
    path('requisitions/', RequisitionsView.as_view()),
    path('staff-requisitions/', StaffRequisitionsView.as_view()),
    path('requisitions/<uuid:pk>/', RequisitionDetailsView.as_view()),
    path('requisitions/process/<uuid:pk>/', RequisitionUpdateView.as_view()),
    # path('inventory/create/', CreateRequisitionView.as_view()),
    # path('inventory/update/<uuid:pk>/', UpdateRequisitionView.as_view()),


]
