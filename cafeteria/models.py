from django.db import models

# Create your models here.
from custom_auth.models import User
from utils.models import TimeStampedModel

PASS_STATUS = (
    ("PENDING", 'PENDING'),
    ("PROCESSED", 'PROCESSED'),
    ("PAID", 'PAID'),
    ("UNPAID", 'UNPAID'),
)


class MealPass(TimeStampedModel):
    meal_time = models.CharField(max_length=60)
    items = models.CharField(max_length=60)
    price = models.DecimalField(decimal_places=2, max_digits=33)
    quantity_remaining = models.IntegerField()
    for_date = models.DateField(null=True)


class UserPass(TimeStampedModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    pass_ref_number = models.CharField(max_length=60, null=True)
    meal_pass = models.ForeignKey(MealPass, on_delete=models.CASCADE, null=True)
    phone_number = models.CharField(max_length=255, null=True)
    status = models.CharField(choices=PASS_STATUS, max_length=255, default="PENDING")
    signature = models.TextField(null=True)



class PassRefNumber(TimeStampedModel):
    pass_ref_number = models.CharField(max_length=255, unique=True)
