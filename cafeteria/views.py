import ast
import datetime
from itertools import groupby

import cryptography
from django.db import transaction
from django.db.models import Count, F
from django.db.models.functions import TruncDay
from django.forms import model_to_dict
from django.shortcuts import render, get_object_or_404

# Create your views here.
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.generics import CreateAPIView, ListAPIView, UpdateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from cafeteria.models import MealPass, UserPass
from cafeteria.serializers import MealPassSerializer, UserPassSerializer
from utils.c import decrypt_message, encrypt
from utils.permissions import IsAdmin, IsChef, IsUser, IsWaiter, IsAuthenticated


class CreateMealPassView(CreateAPIView):
    serializer_class = MealPassSerializer
    queryset = MealPass.objects.all()
    permission_classes = (IsAdmin | IsChef,)


class UpdateMealPassView(UpdateAPIView):
    serializer_class = MealPassSerializer
    queryset = MealPass.objects.all()
    permission_classes = (IsAdmin | IsChef,)


class ViewMealPasses(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = MealPassSerializer

    def get(self, request):
        serializer = MealPassSerializer(MealPass.objects.all(), many=True)
        result = {v["for_date"]: [] for v in serializer.data}
        for v in serializer.data:
            if v:
                if v["for_date"]:
                    result[v["for_date"]].append(v)
        return Response(result)


class BuyMealPass(CreateAPIView):
    permission_classes = (IsAdmin | IsChef | IsUser,)
    serializer_class = UserPassSerializer


class MyMealPasses(ListAPIView):
    permission_classes = (IsAdmin | IsChef | IsUser,)
    serializer_class = UserPassSerializer

    def get(self, request):
        ob = UserPass.objects.filter(user=self.request.user)
        serializer = UserPassSerializer(instance=ob, many=True,  context={'request': self.request})
        result = {v["meal_pass_details"]["for_date"]: [] for v in serializer.data}
        for v in serializer.data:
            if v:
                if v["meal_pass_details"]["for_date"]:
                    result[v["meal_pass_details"]["for_date"]].append(v)
        return Response(result)


class VerifyMealPass(APIView):
    permission_classes = (IsWaiter | IsAdmin | IsChef,)
    serializer_class = UserPassSerializer

    def post(self, request, *args, **kwargs):
        if "signature" not in request.data:
            return Response({"details": "You must pass the unique signature for the order."},
                            status=status.HTTP_400_BAD_REQUEST)
        if "pass_id" not in request.data:
            return Response({"details": "You must pass the unique id for the meal pass."},
                            status=status.HTTP_400_BAD_REQUEST)

        try:

            decrypted_order = ast.literal_eval(decrypt_message(request.data["signature"].encode()).decode())
        except cryptography.fernet.InvalidToken:
            return Response({"error": "The order is could not be verified."}, status=status.HTTP_400_BAD_REQUEST)

        queryset = get_object_or_404(UserPass, pk=request.data["pass_id"], signature=request.data["signature"])
        yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
        if queryset.created < yesterday:
            return Response({"error": "The meal pass is expired."}, status=status.HTTP_400_BAD_REQUEST)
        d = {str(k): str(v) for k, v in model_to_dict(queryset).items() if str(k) != "signature"}
        if "pass_ref_number" in decrypted_order:
            if d["pass_ref_number"] == decrypted_order["pass_ref_number"]:
                serializer_class = UserPassSerializer(instance=queryset, context={'request': self.request})

                return Response({"meal_pass": serializer_class.data}, status=status.HTTP_200_OK)
        else:
            return Response({"error": "The meal pass is could not be verified."}, status=status.HTTP_400_BAD_REQUEST)


class AddSig(UpdateAPIView):
    permission_classes = (IsWaiter | IsAdmin | IsChef,)
    serializer_class = UserPassSerializer
    queryset = UserPass.objects.all()

    @transaction.atomic
    def update(self, request, *args, **kwargs):
        order = get_object_or_404(UserPass, pk=kwargs["pk"])
        d = str({str(k): str(v) for k, v in model_to_dict(order).items() if str(k) != "signature"})
        order.signature = encrypt(d).decode()
        order.save()
        return Response({"details": "Signature refreshed."})


class AddSigToAll(ModelViewSet):
    permission_classes = (IsWaiter | IsAdmin | IsChef,)
    serializer_class = UserPassSerializer

    @action(detail=True, methods=['post'])
    def post(self, request):
        yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
        ordrs = UserPass.objects.filter(created__gt=yesterday, created__lt=datetime.datetime.now())
        with transaction.atomic():
            for i in ordrs:
                d = str({str(k): str(v) for k, v in model_to_dict(i).items() if str(k) != "signature"})
                i.signature = encrypt(d).decode()
                i.save()
        return Response("Signature refreshed.")
