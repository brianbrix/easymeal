import datetime

import django.core.exceptions
from django.db.transaction import atomic
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

import transaction
from cafeteria.models import MealPass, UserPass, PASS_STATUS
from custom_auth.models import User
from utils.helpers import get_serializer_data


def present_or_future_date(value):
    if value < datetime.date.today():
        raise ValidationError("The date cannot be in the past!")
    return value


class MealPassSerializer(serializers.ModelSerializer):
    meal_time = serializers.CharField(required=True, write_only=False)
    items = serializers.CharField(required=True)
    price = serializers.DecimalField(33, 2, required=True)
    quantity_remaining = serializers.IntegerField()
    for_date = serializers.DateField(format="%d-%m-%Y", input_formats=['%d-%m-%Y'], validators=[present_or_future_date])

    class Meta:
        model = MealPass
        fields = '__all__'
        ref_name = "meal_pass"

    # def create(self, validated_data):
    #     """
    #     Create and return a new `MenuItem` instance, given the validated data.
    #     """
    #     validated_data["new_price"] = validated_data["initial_price"]
    #     return MenuItem.objects.create(**validated_data)

    # def update(self, instance, validated_data):
    #     """
    #     Update and return an existing `MenuItem` instance, given the validated data.
    #     """
    #     if "category" in validated_data:
    #         instance.category = validated_data.pop('category')
    #     if "name" in validated_data:
    #         instance.name = validated_data.get('name', instance.name)
    #     if "new_price" in validated_data:
    #         instance.new_price = validated_data.get('new_price', instance.new_price)
    #     if "image" in validated_data:
    #         instance.image = validated_data.get('image', instance.image)
    #
    #     instance.save()
    #     return instance


class UserPassSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), required=False, write_only=False, many=False)
    meal_pass = serializers.PrimaryKeyRelatedField(queryset=MealPass.objects.all(), many=False, required=False)
    status = serializers.ChoiceField(choices=PASS_STATUS, write_only=False, default="PENDING")
    pass_ref_number = serializers.CharField(required=False)
    phone_number = serializers.CharField(required=False)

    meal_pass_details = serializers.SerializerMethodField("meal_pass_det")

    class Meta:
        model = UserPass
        fields = '__all__'

    def meal_pass_det(self, obj):
        request_method = self.context['request'].method
        print(request_method)
        if isinstance(obj, list):
            ls = []
            for x in obj:
                ls.append(get_serializer_data(MealPass, x.meal_pass, MealPassSerializer))
            return ls
        else:
            return get_serializer_data(MealPass, obj.meal_pass, MealPassSerializer)

    @atomic
    def create(self, validated_data):
        if "phone_number" not in validated_data:
            raise ValidationError("The phone number was not specified")
        if "meal_passes" not in self.context.get("request", None).data:
            raise ValidationError("No meal_passes passed as a list")
        if not isinstance(self.context.get("request", None).data["meal_passes"], list):
            raise ValidationError("The m,eal passes must be a list")
        user = self.context.get("request", None).user
        passes = UserPass.objects.filter(user=user, )

        try:
            try:
                ls = []
                ls2 = []
                for x in self.context.get("request", None).data["meal_passes"]:
                    ls2.append(MealPass.objects.filter(id=x).first())
                    ls.append(UserPass(user=user, meal_pass=MealPass.objects.filter(id=x).get(),
                                       phone_number=validated_data["phone_number"]))
                passes = UserPass.objects.filter(user=user, meal_pass__in=ls2)
                ls3 = list(set(str(x.meal_pass.id) for x in passes))
                if len(passes) > 0:
                    raise ValidationError({"error": f"You have already bought the meal passes", "passes":ls3})
                with atomic():
                    for store in ls:
                        store.save()
                return ls
            except MealPass.DoesNotExist as e:
                raise ValidationError(e)
        except django.core.exceptions.ValidationError as e:
            raise ValidationError(e)
