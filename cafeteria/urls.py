from django.urls import path

from .views import *

urlpatterns = [
    path('admin/create/', CreateMealPassView.as_view()),
    path('admin/edit/<uuid:pk>/', UpdateMealPassView.as_view()),
    path('items', ViewMealPasses.as_view()),
    path('meal-pass/buy/', BuyMealPass.as_view()),
    path('my/meal-passes/', MyMealPasses.as_view()),
    path('meal-pass/verify/', VerifyMealPass.as_view()),
    path('meal-pass/ref-sig/<uuid:pk>/', AddSig.as_view()),
    path('meal-pass/ref-sig/all/', AddSigToAll.as_view({"post": "post"})),
    # path('requisitions/create/', CreateRequisitionView.as_view()),


]
