import uuid

from django.db import models

# Create your models here.
from cafeteria.models import MealPass, UserPass
from custom_auth.models import User
from hotel.models import Order
from utils.models import TimeStampedModel


class Invoice(TimeStampedModel):
    # for foreinkey to allow for null pass both blank and null
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, null=True, blank=True)
    meal_pass = models.ForeignKey(UserPass, on_delete=models.CASCADE, null=True, blank=True)
    order_type = models.CharField(choices=(("MEAL_PASS", "MEAL_PASS"), ("RESTAURANT", "RESTAURANT")), null=True,
                                  max_length=60)
    status = models.CharField(choices=(("PENDING", "PENDING"), ("CONFIRMED", "CONFIRMED")), default="PENDING",
                              max_length=60)
    amount = models.DecimalField(decimal_places=2, max_digits=33, default=0)
    invoice_number = models.CharField(unique=True, null=True, max_length=60)


class Transaction(TimeStampedModel):
    amount = models.DecimalField(null=True, decimal_places=2, max_digits=33)
    phone_number = models.CharField(max_length=255, null=True)
    bill_ref_no = models.CharField(max_length=255, null=True)
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE, null=True)


class MealPassTransaction(TimeStampedModel):
    meal_pass = models.ForeignKey(UserPass, on_delete=models.PROTECT)
    transaction = models.ForeignKey(Transaction, on_delete=models.PROTECT)


class OrderTransaction(TimeStampedModel):
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    transaction = models.ForeignKey(Transaction, on_delete=models.PROTECT)
