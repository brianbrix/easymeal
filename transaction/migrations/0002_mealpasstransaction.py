# Generated by Django 3.2 on 2021-05-03 12:19

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('cafeteria', '0008_userpass_phone_number'),
        ('transaction', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MealPassTransaction',
            fields=[
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('meal_pass', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='cafeteria.mealpass')),
                ('transaction', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='transaction.transaction')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
