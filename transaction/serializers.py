from django.db.transaction import atomic
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from cafeteria.models import UserPass
from hotel.models import Order
from transaction.models import Invoice


class InvoiceSerializer(serializers.ModelSerializer):
    order = serializers.PrimaryKeyRelatedField(queryset=Order.objects.all(), required=False)
    meal_pass = serializers.PrimaryKeyRelatedField(queryset=UserPass.objects.all(), required=False)
    order_type = serializers.ChoiceField(choices=(("MEAL_PASS", "MEAL_PASS"), ("RESTAURANT", "RESTAURANT")))
    status = serializers.ChoiceField(choices=(("PENDING", "PENDING"), ("CONFIRMED", "CONFIRMED")))
    amount = serializers.DecimalField(decimal_places=2, max_digits=33)
    invoice_number = serializers.CharField(write_only=False)

    class Meta:
        model = Invoice
        fields = "__all__"

    @atomic
    def update(self, instance, validated_data):
        if instance.status == "CONFIRMED":
            raise ValidationError({"error":"The invoice is already confirmed."})
        if "status" in validated_data:
            validated_data.pop("status")
        if "amount" not in validated_data:
            raise ValidationError({"error": "You must pass an amount."})
        if validated_data["amount"] == instance.amount:
            instance.status = "CONFIRMED"
            instance.save()
        else:
            raise ValidationError({"error": "Invalid amount given."})
        return instance
