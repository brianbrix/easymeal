import random
import string

from django.db import transaction
from django.forms import model_to_dict
from rest_framework.exceptions import ValidationError

from transaction.models import Transaction, OrderTransaction, MealPassTransaction, Invoice
from utils.c import encrypt


def generate_number(size=5):
    code = 'INV-' + ''.join(random.choice(string.ascii_letters[26:] + string.digits) for i in range(size))
    inv = Invoice.objects.filter(invoice_number=code).first()
    if inv:
        return generate_number(size=5)
    return code


@transaction.atomic
def makePayment(instance):
    try:
        d = str({str(k): str(v) for k, v in model_to_dict(instance).items() if str(k) != "signature"})
        encrypted = encrypt(d)
        instance.signature = encrypted.decode()
        instance.save()
        i = Invoice.objects.create(order=instance, order_type="RESTAURANT", invoice_number=generate_number(6),
                                   amount=instance.paid_amount, user=instance.user)
        i.full_clean()
        # t = Transaction.objects.create(amount=instance.paid_amount, phone_number=instance.phone_number,
        #                                bill_ref_no=instance.order_number)
        # t.full_clean()
        # OrderTransaction.objects.create(order=instance, transaction=t)
    except ValidationError as e:
        pass


@transaction.atomic
def makePassPayment(instance):
    try:
        d = str({str(k): str(v) for k, v in model_to_dict(instance).items() if str(k) != "signature"})
        encrypted = encrypt(d)
        instance.signature = encrypted.decode()
        instance.save()

        i = Invoice.objects.create(meal_pass=instance, order_type="MEAL_PASS", invoice_number=generate_number(6),
                                   amount=instance.meal_pass.price, user=instance.user)
        i.full_clean()
        # t = Transaction.objects.create(amount=instance.meal_pass.price, phone_number=instance.phone_number,
        #                                bill_ref_no=instance.pass_ref_number)
        # t.full_clean()
        # MealPassTransaction.objects.create(meal_pass=instance, transaction=t)
    except ValidationError as e:
        pass
