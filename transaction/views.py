from django.shortcuts import render

# Create your views here.
from rest_framework.generics import UpdateAPIView, ListAPIView

from transaction.models import Invoice
from transaction.serializers import InvoiceSerializer
from utils.permissions import IsUser, IsAdmin, IsAuthenticated


class ProcessInvoiceView(UpdateAPIView):
    serializer_class = InvoiceSerializer
    permission_classes = (IsUser | IsAdmin,)
    queryset = Invoice.objects.all()


class ListAllInvoices(ListAPIView):
    serializer_class = InvoiceSerializer
    permission_classes = (IsAuthenticated,)
    queryset = Invoice.objects.all().order_by("-created")


class ListMyInvoices(ListAPIView):
    serializer_class = InvoiceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = Invoice.objects.filter(user=self.request.user).order_by("-created")
        return queryset
